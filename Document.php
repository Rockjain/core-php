<?php
namespace Src\Controllers;

use Google_Client;
use Google_Service_Drive;
use Src\Str;
use Src\File;
use Src\Mail;
use Src\Auth;
use Src\Testclass;
use Src\Database;

use TCPDF2DBarcode;
use Src\MergeQR;

class Document{

    private $fdata;

    /**
     * Get documents view
     * 
     * @return \Pecee\Http\Response
     */
    public function get() {
    
        $user = Auth::user();
        $role = $user->role; 
        $headerBtn = "";

        $headerBtn .= '<div class="pull-right1 page-actions">
                    <button href="" class="btn btn-default go-back hidden-xs"><i class="ion-ios-arrow-back"></i> Back</button>
                    <a href='.url('Settings@get').' class="btn btn-primary hidden-xs"><i class="ion-edit"></i>Settings</a>
                    <a href='.url('Document@get').' class="btn btn-primary"><i class="ion-document-text"></i>Documents</a>
                    </div>';

        $doc_view = Database::table("document_view")->where("user_id", $user->id)->first();

        if($doc_view === NULL){
            $view_style = 0;
        }else{
            $view_style = $doc_view->doc_view;
        }

        $page_title = "Documents";
        $title_muted = "Home Folder";

        if($user->company){
            $alltags = Database::table("tagmaster")->where("company_id", $user->company)->get();
        }else{
            $alltags = Database::table("tagmaster")->where("parent", $user->parent)->get();
        }

        return view('documents', compact("user","role","view_style","headerBtn", "page_title", "title_muted", "alltags"));
    }

    /**
     * Get documents view
     * 
     * @return \Pecee\Http\Response
     */
    public function open($document_key) {
        $headerBtn = '';

        $user = Auth::user();
        $requestPositions = json_encode(array());
        $requestWidth = 0;
        $company = Database::table("companies")->where("id", $user->company)->first();
        $document = Database::table("files")->where("document_key", $document_key)->first();
        if (empty($document)) {
            return view('errors/404');   
        }
        if ($document->is_template == "Yes") { 
          $lauchLabel = "Manage Fields & Edit"; 
          $template_fields = json_decode($document->template_fields, true);
          $savedWidth = $template_fields[0];
          if (empty($savedWidth)) { $savedWidth = 0; }
        }else{ 
          $lauchLabel = "Sign & Edit"; 
          $template_fields = json_encode(array());
          $savedWidth = 0;
        }
        if (isset($_GET['signingKey'])) {
          $signingKey = $_GET['signingKey'];
          $request = Database::table("requests")->where("signing_key", $signingKey)->first();
          $clientIP = Testclass::get_client_ip();
          $activity = '<span class="text-primary">'.escape($user->fname." - ".$request->email).'['.$clientIP.']</span> opened this document.';
          Testclass::keephistory($request->document, $activity, "default");
          if (!empty($request->positions)) {
            $requestPositions = json_decode($request->positions, true);
            $requestWidth = $requestPositions[0];
            $requestPositions = json_encode($requestPositions, true);
            if (empty($requestWidth)) { $requestWidth = 0; }
          }
        }else{
          $request = '';
        }
        $templateFields = json_encode($template_fields, true);

        if($user->company){
            $customers = Database::table("users")->where("company", $user->company)->where("role", "user")->get();
        }else{
            $customers = Database::table("users")->where("parent", $user->parent)->where("role", "user")->get();
        }

        if($user->company){
            $team = Database::table("users")->where("company", $user->company)->where("role", "staff")->get();
        }else{
            $team = Database::table("users")->where("parent", $user->parent)->where("role", "staff")->get();
        }

        $fields = Database::table("fields")->where("user", $user->id)->where("type", "custom")->orderBy("id", false)->get();
        $stamps = Database::table("fields")->where("user", $user->id)->where("type", "stamp")->orderBy("id", false)->get();
        $inputfields = Database::table("fields")->where("user", $user->id)->where("type", "input")->orderBy("id", false)->get();
        $history = Database::table("history")->where("file", $document->document_key)->get();
        $chats = Database::table("chat")->where("file", $document_key)->leftJoin("users", "users.id","chat.sender")->get("`chat.id`", "`chat.message`", "`chat.time_`", "`chat.sender`", "`users.avatar`", "`users.fname`", "`users.lname`");

        $noticequery = (string)'SELECT a.*, b.avatar, b.fname, b.lname FROM `notice` a, users b WHERE a.`sender` = b.`id` AND a.document_key="'.$document->document_key.'"';
        $notice = Database::table("users, notice")->queryFire("$noticequery");
        
        if(!empty($notice)){
            foreach($notice as $noticeTimeUpdate){
                $noticeTimeUpdate->time = Testclass::userTimeConvert($user->timezone, $noticeTimeUpdate->time, 'Y-m-d H:i:s');
            }
        }

        if(!empty($history)){
            foreach($history as $historyTimeUpdate){
                $historyTimeUpdate->time_ = Testclass::userTimeConvert($user->timezone, $historyTimeUpdate->time_, 'Y-m-d H:i:s');
            }
        }

        $alltags1 = array();
        $alltags2 = array();

        $alltags1 = Database::table("tagmaster")->where("created_by", $user->id)->get();
        if($user->role == "user"){
            if($user->company){
                $alltags2 = Database::table("tagmaster")->where("company_id", $user->company)->get();
            }else{
                $alltags2 = Database::table("tagmaster")->where("parent", $user->parent)->get();
            }
        }
        $alltags3 = array_merge($alltags1,$alltags2);

        $alltags = array();
        foreach ($alltags3 as $tag) {
            $alltags[$tag->tag_id] = $tag;
        }

        $alltags = array_values($alltags);

        if($user->company == 0){
            $query = "SELECT `tag_id` FROM `tagdocuments` WHERE `document_id` = $document->id AND `company_id` = $user->parent";
        }else{
            $query = "SELECT `tag_id` FROM `tagdocuments` WHERE `document_id` = $document->id AND `company_id` = $user->company";
        }

        $assigntags = Database::table("tagdocuments")->queryFire($query);
        $atag = array(); // atag = assign tags
        foreach ($assigntags as $index => $value) {
          $atag[] = $value->tag_id;
        }

        $currentpath = realpath(config("app.storage")."files/".$document->filename);
        $currentHash  = hash_file('sha512' , $currentpath);
        if($currentHash == $document->latest_hash){

          $hashmessage = '<div class="text-muted1 hashValid"><div><img src="/dna/assets/images/valid.png?t" style="width: 30px;"><span style="padding-right:15px; width:40%;"> ORIGINAL AND CERTIFIED FILE - INTEGRITY PASSED </span></div></div>';
        }else{
          $hashmessage = '<div class="text-muted1 hashInvalid"><div><img src="/dna/assets/images/invalid.png?t" style="width: 30px;"><span style="padding-right:15px; width:40%;"> WARRING !!!! FAKE FILE !!!! </span></div></div>';
        }
        $src='https://view.officeapps.live.com/op/embed.aspx?src='.env("APP_URL").'/uploads/files/'.$document->filename.'?d='.time();

        
        $headerBtn .=
        '<div class="pull-right1 page-actions">
        
            <button class="btn btn-primary hash-request1" data-toggle="modal" data-target="#addTag" data-backdrop="static" data-keyboard="false" style="margin-right: 2px;"><i class="ion-ios-tag-outline"></i> <span>Add Edit Tags</span></button>';
            
            if ( is_object($request) && $request->status == "Pending" ){
            $headerBtn .=
            '<button class="btn btn-success accept-request"><i class="ion-ios-checkmark-outline"></i> <span>Accept</span> </button>';

            $headerBtn .=
            '<div class="dropdown">
                <button class="btn btn-danger dropdown-toggle btn-responsive" data-toggle="dropdown"><i class="ion-arrow-down-b"></i> Decline </button>
                <ul class="dropdown-menu document-menu">';
                        $headerBtn .=
                        '<li><a href="" class="send-to-server-click"  data="requestid:'. $request->id .'|file:'. $document->id .'|csrf-token:'.csrf_token().'" url="'.url("Request@decline").'" warning-title="Are you sure?" warning-message="This request will be declined and email sent to sender." warning-button="Yes, Decline" loader="true"><i class="ion-ios-close-outline"></i><span> Decline </sapn></a></li>';
                        $headerBtn .=
                        '<li><a href="" data-toggle="modal" data-target="#declineMessage" data-backdrop="static" data-keyboard="false"><i class="ion-ios-close-outline"></i><span> Decline with message</span></a></li>';
                    
                $headerBtn .=
                '</ul>
            </div>';

            }else
            if ( in_array("edit",json_decode($user->permissions)) ){
                if ( $document->extension != "pdf" ){
                $headerBtn .=
                '<button class="btn btn-success btn-responsive send-to-server-click"  data="source:viewer|document_key:'. $document->document_key .'|csrf-token:'. csrf_token() .'" url="'.url("Conversion@ConvertFile").'" warning-title="Convert?" warning-message="This file should be converted to PDF before signing or editing. Click convert to proceed.." warning-button="Convert" loader="true"><i class="ion-edit"></i>'. $lauchLabel .'</button>';
                }elseif(!isset($_GET['signingKey'])){
                    $headerBtn .=
                    '<button class="btn btn-success btn-responsive launch-editor"><i class="ion-edit"></i>'. $lauchLabel .'</button>';
                }
            }
            
            if ( $document->company ==  $user->company ){
            $headerBtn .=
            '<div class="dropdown">
                <button class="btn btn-primary dropdown-toggle btn-responsive" data-toggle="dropdown"><i class="ion-arrow-down-b"></i> More </button>
                <ul class="dropdown-menu document-menu">';
                    if ( $document->is_template == "No" ){
                        $headerBtn .=
                        '<li><a href=""  data-toggle="modal" data-target="#sendFile" data-backdrop="static" data-keyboard="false"><i class="ion-ios-email-outline"></i> <span>Send</span></a></li>
                        <li class="divider"></li>';
                    }
                    if ( $document->extension == "pdf" ){
                        $headerBtn .=
                        '<li><a href="" data-toggle="modal" data-target="#sendRequest" data-backdrop="static" data-keyboard="false"><i class="ion-ios-plus-outline"></i><span> Request Sign</span></a></li>
                        <li class="divider"></li>';
                    }
                    if ( $document->is_template == "No" ){
                        $headerBtn .=
                        '<li><a href="" data-toggle="modal" data-target="#protectFile" data-backdrop="static" data-keyboard="false">
                        <i class="ion-ios-locked-outline"></i> <span>Protect</span></a></li>
                        <li class="divider"></li>';
                    }
                    $headerBtn .=
                    '<li><a href="" data-toggle="modal" data-target="#replaceFile" data-backdrop="static" data-keyboard="false"> <i class="ion-ios-browsers-outline"></i><span> Replace</span></a></li>
                    <li class="divider"></li>';

                    if ( $document->status == "Signed" || $document->editted == "Yes" ){
                        $headerBtn .=
                        '<li data-toggle="tooltip" data-placement="left" data-original-title="Restore original version"><a href="" class="send-to-server-click"  data="file:'. $document->id .'|csrf-token:'. csrf_token() .'" url="'.url("Document@restore").'" warning-title="Are you sure?" warning-message="The original file will be restored and this action is irreversable." warning-button="Continue" loader="true"><i class="ion-ios-loop"></i><span> Restore</span></a></li>
                        <li class="divider"></li>
                        <li><a href=""  data-toggle="modal" data-target="#downloadOptions" data-backdrop="static" data-keyboard="false"><i class="ion-ios-cloud-download-outline"></i><span> Download</span></a></li>
                        <li class="divider"></li>';
                    }else{
                        $headerBtn .=
                        '<li><a href="'.url('') .'uploads/files/'.$document->filename.'" download="'.$document->name.'.'.$document->extension.'"><i class="ion-ios-cloud-download-outline"></i><span> Download</span></a></li>
                        <li class="divider"></li>';
                    }
                    if ( $document->extension != "pdf" ){
                        $headerBtn .='<li data-toggle="tooltip" data-placement="left" data-original-title="Convert To PDF"><a href="" class="send-to-server-click"  data="document_key:'. $document->document_key .'|csrf-token:'.csrf_token().'" url="'.url("Conversion@ConvertFile").'" warning-title="Are you sure?" warning-message="This file will be converted to PDF and this action is irreversable." warning-button="Continue" loader="true"><i class="ion-usb"></i><span> Convert</span></a></li>
                                        <li class="divider"></li>';
                    }
                    if ( in_array("delete",json_decode($user->permissions)) ){
                        $headerBtn .='<li><a href="" class="send-to-server-click"  data="source:viewer|file:'.$document->id.'|csrf-token:'.csrf_token().'" url="'.url("Document@deletefile").'" warning-title="Are you sure?" warning-message="This file will be deleted and cant be recovered." warning-button="Continue" loader="true"><i class="ion-ios-trash-outline"></i> <span>Delete</span></a></li>';
                    }
                $headerBtn .=
                '</ul>
            </div>';
           }
           
        $headerBtn .=
        '</div>';

        $page_title = "Document";
        $title_muted = $document->name;

        $uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $uri_segments = explode('/', $uri_path);

        $date_utc = new \DateTime("now", new \DateTimeZone("UTC"));
        $UTCtime = $date_utc->format('Y-m-d H:i:s');

        $signatureFileDetail = getimagesize(config("app.storage")."signatures/".$user->signature);
        // echo "<pre>"; print_r($checkk[0]); die();

        return view('sign', compact("user", "document", "history", "customers", "team", "chats", "company","fields","inputfields","templateFields","savedWidth","lauchLabel","request","requestWidth","requestPositions","stamps","hashmessage","alltags","atag", "notice","headerBtn","page_title","title_muted","UTCtime", "signatureFileDetail"));
    }

    /**
     * Download a file
     * 
     * @return \Pecee\Http\Response
     */
    public function download($docId) {
        $document = Database::table("files")->where("id", $docId)->first();
        $file = config("app.storage")."files/".$document->filename;
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="'.$document->name.'.'.$document->extension.'"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
        flush();
        readfile($file);
        exit();
    }

    /**
     * Get documents view
     * 
     * @return \Pecee\Http\Response
     */
    public function fetch() {

        $user = Auth::user();
        if(isset($_COOKIE['item-count'])){
            $limit = $_COOKIE['item-count'];
        }else{
            $limit = 20;
        }

        if(input('sort-column') != ""){
            $sortColumn = input('sort-column');
        }else{
            $sortColumn = "name";
        }

        if(input('sort-order') != ''){
            $sortOrder .= input('sort-order');
            $sortOrderClass = input('sort-order');
        }else{
            $sortOrder .= "ASC";
            $sortOrderClass = "ASC";
        }

        $orderBy = " AND `is_archive` = 0 GROUP BY `document_key`  ORDER BY ".$sortColumn." ".$sortOrder;

        if(isset($_POST['page']) && $_POST['page'] != ''){
            $page = $_POST['page'];
            $startFrom = ($page - 1) * $limit;
        }else{
            $page = 1;
            $startFrom = 0;
        }
            $endPoint = $startFrom+$limit;

        if(!isset($_POST['c'])){
           $c = 0;
           $cp = $page;
        }else{
           $c = $_POST['c'];
           $cp = $_POST['cp'];
        }

        $thisFolder = Database::table("folders")->where("id", input("folder"))->first();
        $hiddenFiles = json_decode($user->hiddenfiles);
        $folders = $documents = array();
        if (!empty($thisFolder->password)) {
            if (empty(input("password"))) {
                $protected = true;
                $incorrect = false;
                return view('extras/documents', compact("user", "protected", "incorrect"));
            }else{
                if(!hash_compare($thisFolder->password, Auth::password(input("password")))){
                    $incorrect = $protected = true;
                    return view('extras/documents', compact("user", "protected", "incorrect"));
                }
            }
        }
        if (empty($_POST['type']) || $_POST['type'] == "folders") {
            $foldersQuery = "SELECT * FROM folders ";

            if (!empty(input("search"))) {
                $foldersQuery .= " WHERE name LIKE '%".input("search")."%'"."  AND created_by = ".$user->id;
            }else{
                $foldersQuery .= " WHERE folder = ".input("folder")."  AND created_by = ".$user->id;
            }

            if ($user->role == "user" && $folder->created_by != $user->id) {
                $foldersQuery .= " AND created_by = ".$user->id;
            }

            $foldersQuery .= $orderBy;

            $foldersQuery = str_replace("document_key","id",$foldersQuery);
            $foldersCount = Database::table("folders")->queryFire($foldersQuery);

        }
        if (empty($_POST['type']) || $_POST['type'] == "files") {
            $sharedFileKeys = array();
            $requestsData = Database::table("requests")->where("receiver", $user->id)->orWhere("email", $user->email)->orderBy("id", false)->get();

            $documentsQueryStatus = $documentsQueryExtension = $documentsQueryUser = $documentsQueryHiddenFiles = '';
            if(isset($hiddenFiles) and $hiddenFiles != null){
                if(input('search') != ''){
                    $documentsQueryHiddenFiles = " AND ( a.id NOT IN ('".implode("', '", @$hiddenFiles)."')";
                }else{
                    $documentsQueryHiddenFiles = " AND ( id NOT IN ('".implode("', '", @$hiddenFiles)."')";
                }
            }else{
                if(input('search') != ''){
                    $documentsQueryHiddenFiles = " AND ( a.id NOT IN ('')";
                }else{
                    $documentsQueryHiddenFiles = " AND ( id NOT IN ('')";
                }
            }
            $documentsQueryHiddenFiles .= " OR document_key IN ('".implode("', '", $sharedFileKeys)."') )";

            if (!empty($_POST['status'])) {
                $documentsQueryStatus = " AND status = ".$_POST['status']." ";
            }
            if (!empty($_POST['extension'])) {
                $documentsQueryExtension = " AND ( extension = '".$_POST['extension']."' OR extension = '".$_POST['extension']."x' )"." ";
            }
            if ($user->role == "user" && $document->uploaded_by != $user->id) {
                $documentsQueryUser = " AND uploaded_by = ".$user->id." ";
            }

            if (!empty(input("search"))) {
                $search = input("search");
                $where = 'a.`uploaded_by` = "'.$user->id.'"';

                $documentsQuery = (string)'SELECT a.* FROM `files` a LEFT JOIN `tagdocuments` c on c.`document_id` = a.`id` LEFT JOIN `tagmaster` b on b.`tag_id` = c.`tag_id` WHERE '.$where.' '.$documentsQueryStatus.$documentsQueryExtension.$documentsQueryUser.$documentsQueryHiddenFiles.' AND (b.`tagname` LIKE "%'.$search.'%" OR a.`name` LIKE "%'.$search.'%") '. $orderBy;
                
            }else{
                if($user->company==0){
                    $documentsQuery = "SELECT * FROM files WHERE uploaded_by = ".$user->id." AND folder = ".input("folder"). " AND is_template = 'No'  ".$orderBy;
                }else{
                    $documentsQuery = "SELECT * FROM files WHERE uploaded_by = ".$user->id." AND folder = ".input("folder"). " AND is_template = 'No' " .$documentsQueryStatus. $documentsQueryExtension . $documentsQueryUser . $documentsQueryHiddenFiles . $orderBy;
                }
            }

            $documentsCount = Database::table("files")->queryFire($documentsQuery);
        }
        
        $fileCount      = count($documentsCount);
        $folderCount    = count($foldersCount);

        if($folderCount >= $endPoint){
            $foldersQuery .= " LIMIT ".$startFrom." , ".$limit;

            $foldersQuery = str_replace("document_key","id",$foldersQuery);
            $folders = Database::table("folders")->queryFire($foldersQuery);
            
            $documents = array();
        }elseif($folderCount < $endPoint && $folderCount > $startFrom) {
            $newLimit = $folderCount-$startFrom;
            $foldersQuery .= " LIMIT ".$startFrom." , ".$newLimit;

            $foldersQuery = str_replace("document_key","id",$foldersQuery);
            $folders = Database::table("folders")->queryFire($foldersQuery);

            $documentsQuery .= " LIMIT 0 , ";
            $documentsQuery .= ($limit-$newLimit);

            $documents = Database::table("files")->queryFire($documentsQuery);
            $c = $limit-$newLimit;
            $cp = $page;
        }else{
            $folders = array();

            if($c == 0 && $page != 1){
                $s = ($page - 1)*$limit;
            }else{
                if($folderCount == 0){
                    $s = ($page-1)*$limit;
                }else{
                    $s = ($page - $cp)*$c;    
                    
                    $a = ($page * $limit) - $limit;
                    $b = (($cp*$limit) - $c);

                    $s = $a-$b;
                }
            }

            $documentsQuery .= " LIMIT ".$s." , ".$limit;

            $documents = Database::table("files")->queryFire($documentsQuery);
        }
            $total = $folderCount+$fileCount;
            $html = '';
            if($total > 0){

                if($limit > $total){
                    $quotient = 0;
                    $remainder = 0;
                }else{
                    $remainder = $total % $limit;
                    $quotient = ($total - $remainder) / $limit;;
                }

                if($remainder != 0) {
                    $totalPage = $quotient + 1;
                }else{
                    $totalPage = $quotient;
                }

                if($sortOrder == 'ASC'){
                    $rSortOrder = 'DESC';
                }else{
                    $rSortOrder = 'ASC';
                }

                $sortOrder = "'".$sortOrder."'";
                $sortColumn2 = $sortColumn;
                $sortColumn = "'".$sortColumn."'";
                $rSortOrder = "'".$rSortOrder."'";

                $html = '';
                $currentPage = 1;
                for ($p=1; $p <= $totalPage; $p++) {
                    if($p == $page){$cls="active"; $currentPage = $p; }else{$cls='';}
                    $html .= '<li class="pageLI '.$cls.'"><a onclick="pages('.$p.', event, this, '.$c.', '.$cp.', '.$sortOrder.', '.$sortColumn.')" href1="#"><span>'.$p.'<span></a></li>';
                }

                if($currentPage == 1 ){
                    $previous = '<li class="pageLI corner disabled"><a class="previous disabled"><span>Previous<span></a></li>';
                }else{
                    $previous = '<li class="pageLI corner"><a onclick="pages('.($currentPage-1).', event, this, '.$c.', '.$cp.', '.$sortOrder.', '.$sortColumn.')" href1=""><span>Previous<span></a></li>';
                }

                if($currentPage == $totalPage || $totalPage == 0){
                    $next = '<li class="pageLI corner disabled"><a><span>Next<span></a></li>';
                }else{
                    $next = '<li class="pageLI corner"><a onclick="pages('.($currentPage+1).', event, this, '.$c.', '.$cp.', '.$sortOrder.', '.$sortColumn.')" href1=""><span>Next<span></a></li>';
                }
                if($html === ''){
                    $html .= '<li class="pageLI active"><a onclick="pages(1, event, this, '.$c.', '.$cp.', '.$sortOrder.', '.$sortColumn.')" href1=""><span>'.$p.'<span></a></li>';
                }

                if($totalPage == 1 || $totalPage == 0){
                    $hidePagination = 'style = "display: none;"';
                }else{
                    $hidePagination = "";
                }
                $page_html = '<div class="col-md-12"><ul class="pagination pull-right" '.$hidePagination.'>'.$previous.$html.$next.'</ul></div>';
            }
                        
            $doc_view = Database::table("document_view")->where("user_id", $user->id)->first();

            if($doc_view === NULL){
                $view_style = 0;
            }elseif(input("urisegment") == "dashboard") {
                $view_style = 0;
            }else {
                $view_style = $doc_view->doc_view;
            }

            if($currentPage == 1) {
                $srNo = 0 + 1;
            } else {
                $srNo = (($currentPage - 1)*$limit) + 1;
            }

            $res = array(
                        'html' => view('extras/documentslist', compact("user", "folders", "documents", "view_style", "sortOrder", "sortColumn2", "rSortOrder", "srNo", "sortOrderClass")),
                        'pagination'=> $page_html,
                        'view_style'=> $view_style,

                    );
            exit(json_encode($res));

        return view('extras/documentslist', compact("user", "folders", "documents"));
    }

    public function fetch2() { // use this function to tag search

        $user = Auth::user();
        if(isset($_COOKIE['item-count'])){
            $limit = $_COOKIE['item-count'];
        }else{
            $limit = 20;
        }

        if(input('sort-column') != ""){
            $sortColumn = input('sort-column');
        }else{
            $sortColumn = "name";
        }

        if(input('sort-order') != ''){
            $sortOrder .= input('sort-order');
            $sortOrderClass = input('sort-order');
        }else{
            $sortOrder .= "ASC";
            $sortOrderClass = 'ASC';
        }

        $orderBy = "  AND `is_archive` = 0 ORDER BY ".$sortColumn." ".$sortOrder;

        if(isset($_POST['page']) && $_POST['page'] != ''){
            $page = $_POST['page'];
            $startFrom = ($page - 1) * $limit;
        }else{
            $page = 1;
            $startFrom = 0;
        }
            $endPoint = $startFrom+$limit;

        if(!isset($_POST['c'])){
           $c = 0;
           $cp = $page;
        }else{
           $c = $_POST['c'];
           $cp = $_POST['cp'];
        }

        if (empty($_POST['type']) || $_POST['type'] == "files") {
            $sharedFileKeys = array();
            $requestsData = Database::table("requests")->where("receiver", $user->id)->orWhere("email", $user->email)->orderBy("id", false)->get();

            $documentsQueryStatus = $documentsQueryExtension = $documentsQueryUser = $documentsQueryHiddenFiles = '';
            if(isset($hiddenFiles) and $hiddenFiles != null){
                if(input('search') != ''){
                    $documentsQueryHiddenFiles = " AND ( a.id NOT IN ('".implode("', '", @$hiddenFiles)."')";
                }else{
                    $documentsQueryHiddenFiles = " AND ( id NOT IN ('".implode("', '", @$hiddenFiles)."')";
                }
            }else{
                if(input('search') != ''){
                    $documentsQueryHiddenFiles = " AND ( a.id NOT IN ('')";
                }else{
                    $documentsQueryHiddenFiles = " AND ( a.id NOT IN ('')";
                }
            }
            $documentsQueryHiddenFiles .= " OR document_key IN ('".implode("', '", $sharedFileKeys)."') )";

            if (!empty($_POST['status'])) {
                $documentsQueryStatus = " AND status = ".$_POST['status']." ";
            }
            if (!empty($_POST['extension'])) {
                $documentsQueryExtension = " AND ( extension = '".$_POST['extension']."' OR extension = '".$_POST['extension']."x' )"." ";
            }
            if ($user->role == "user" && $document->uploaded_by != $user->id) {
                $documentsQueryUser = " AND uploaded_by = ".$user->id." ";
            }

            if (!empty(input("tags"))) {
                $search = input("tags");

                $tagArray = explode(',', $search);
                $tagQuery = "";
                foreach ($tagArray as $tag) {
                    $tag = trim($tag);
                    $tagQuery .= ' b.`tagname` LIKE "%'.$tag.'%" OR ';
                }

                $tagQuery = substr_replace($tagQuery ,"",-3);

                $where = 'a.`uploaded_by` = "'.$user->id.'"';
                $documentsQuery = (string)'SELECT a.* FROM `files` a LEFT JOIN `tagdocuments` c on c.`document_id` = a.`id` LEFT JOIN `tagmaster` b on b.`tag_id` = c.`tag_id` WHERE '.$where.' '.$documentsQueryStatus.$documentsQueryExtension.$documentsQueryUser.$documentsQueryHiddenFiles.' AND ( '.$tagQuery.' ) '. $orderBy;
                
            }else{
                if($user->company==0){
                    $documentsQuery = "SELECT * FROM files WHERE uploaded_by= ".$user->id ." AND is_template = 'No'  ".$orderBy;
                }else{
                    $documentsQuery = "SELECT * FROM files WHERE uploaded_by= ".$user->id. " AND is_template = 'No' " .$documentsQueryStatus. $documentsQueryExtension . $documentsQueryUser . $documentsQueryHiddenFiles . $orderBy;
                }
            }

            $documentsCount = Database::table("files")->queryFire($documentsQuery);
        }

        $foldersCount = array();
        
        $fileCount      = count($documentsCount);
        $folderCount    = count($foldersCount);

        if($folderCount >= $endPoint){
            $foldersQuery .= " LIMIT ".$startFrom." , ".$limit;
            $folders = Database::table("folders")->queryFire($foldersQuery);
            
            $documents = array();
        }elseif($folderCount < $endPoint && $folderCount > $startFrom) {
            $newLimit = $folderCount-$startFrom;
            $foldersQuery .= " LIMIT ".$startFrom." , ".$newLimit;
            $folders = Database::table("folders")->queryFire($foldersQuery);

            $documentsQuery .= " LIMIT 0 , ";
            $documentsQuery .= ($limit-$newLimit);

            $documents = Database::table("files")->queryFire($documentsQuery);
            $c = $limit-$newLimit;
            $cp = $page;
        }else{
            $folders = array();

            if($c == 0 && $page != 1){
                $s = ($page - 1)*$limit;
            }else{
                if($folderCount == 0){
                    $s = ($page-1)*$limit;
                }else{
                    $s = ($page - $cp)*$c;    
                    
                    $a = ($page * $limit) - $limit;
                    $b = (($cp*$limit) - $c);

                    $s = $a-$b;
                    /*$s = ($page * $limit) - $limit - ($c($cp*$limit));*/
                }
            }

            $documentsQuery .= " LIMIT ".$s." , ".$limit;

            $documents = Database::table("files")->queryFire($documentsQuery);
        }

        // if(!isset($_POST['page'])){
            $total = $folderCount+$fileCount;
            $html = '';
            if($total > 0){

                if($limit > $total){
                    $quotient = 0;
                    $remainder = 0;
                }else{
                    $remainder = $total % $limit;
                    $quotient = ($total - $remainder) / $limit;;
                }

                if($remainder != 0) {
                    $totalPage = $quotient + 1;
                }else{
                    $totalPage = $quotient;
                }

                if($sortOrder == 'ASC'){
                    $rSortOrder = 'DESC';
                }else{
                    $rSortOrder = 'ASC';
                }

                $sortOrder = "'".$sortOrder."'";
                $sortColumn2 = $sortColumn;
                $sortColumn = "'".$sortColumn."'";
                $rSortOrder = "'".$rSortOrder."'";

                $html = '';
                $currentPage = 1;
                for ($p=1; $p <= $totalPage; $p++) {
                    if($p == $page){$cls="active"; $currentPage = $p; }else{$cls='';}
                    $html .= '<li class="pageLI '.$cls.'"><a onclick="pages('.$p.', event, this, '.$c.', '.$cp.', '.$sortOrder.', '.$sortColumn.')" href1="#"><span>'.$p.'<span></a></li>';
                }

                if($currentPage == 1 ){
                    $previous = '<li class="pageLI corner disabled"><a><span>Previous<span></a></li>';
                }else{
                    $previous = '<li class="pageLI corner"><a onclick="pages('.($currentPage-1).', event, this, '.$c.', '.$cp.', '.$sortOrder.', '.$sortColumn.')" href1=""><span>Previous<span></a></li>';
                }

                if($currentPage == $totalPage || $totalPage == 0){
                    $next = '<li class="pageLI corner disabled"><a><span>Next<span></a></li>';
                }else{
                    $next = '<li class="pageLI corner"><a onclick="pages('.($currentPage+1).', event, this, '.$c.', '.$cp.', '.$sortOrder.', '.$sortColumn.')" href1=""><span>Next<span></a></li>';
                }
                if($html === ''){
                    $html .= '<li class="pageLI active"><a onclick="pages(1, event, this, '.$c.', '.$cp.', '.$sortOrder.', '.$sortColumn.')" href1=""><span>'.$p.'<span></a></li>';
                }

                if($totalPage == 1 || $totalPage == 0){
                    $hidePagination = 'style = "display: none;"';
                }else{
                    $hidePagination = "";
                }
                $page_html = '<div class="col-md-12"><ul class="pagination" '.$hidePagination.'>'.$previous.$html.$next.'</ul></div>';
            }
                        
            $doc_view = Database::table("document_view")->where("user_id", $user->id)->first();

            if($doc_view === NULL){
                $view_style = 0;
            }else{
                $view_style = $doc_view->doc_view;
            }

            if($currentPage == 1) {
                $srNoStrart = 0;
            } else {
                $srNoStrart = $currentPage*$limit;
            }

            $res = array(
                        'html' => view('extras/documentslist', compact("user", "folders", "documents", "view_style", "sortOrder", "sortColumn2", "rSortOrder", "srNoStrart", "sortOrderClass")),
                        'pagination'=> $page_html,
                        'view_style'=> $view_style,

                    );
            exit(json_encode($res));
        // }
        return view('extras/documentslist', compact("user", "folders", "documents"));
    }

    /**
     * Relocate file & folders
     * 
     * @return Json
     */
    public function relocate() {
        header('Content-type: application/json');
        foreach ($_POST['data'] as $data) {
            if ($data["type"] == "folder") {
                Database::table("folders")->where("id", $data["sourceid"])->update("folder", $data["destination"]);
            }else {
                Database::table("files")->where("id", $data["sourceid"])->update("folder", $data["destination"]);
            }
        }
        exit(json_encode(responder("success", "", "","", false)));
    }

    /**
     * Create a folder
     * 
     * @return Json
     */
    public function createfolder() {
        header('Content-type: application/json');
        $user = Auth::user();
        if ($user->company == 0) {
            $folders = Database::table("folders")->where("name", input("name"))->where("folder", input("folder"))->first();
        }else{
            $folders = Database::table("folders")->where("name", input("name"))->where("folder", input("folder"))->where("company", $user->company)->first();
        }
        if (!empty($folders)) {
            exit(json_encode(responder("error", "Already Exists!", "Folder name '".input("name")."' already exists.")));
        }
        if ($user->role == "user") {
            $accessibility = "Only Me";
        }else{
            $accessibility = "Everyone";
        }
        if($user->company){
            $parentID = 0;
        }else{
            $parentID = $user->parent;
        }
        $data = array(
                        "company"   => $user->company,
                        "parent"    => $parentID,
                        "created_by"=> $user->id,
                        "name"      => input("name"),
                        "accessibility" => $accessibility,
                        "folder"    => input("folder"),
                    );
        Database::table("folders")->insert($data);
        exit(json_encode(responder("success", "", "","documentsCallback()", false)));
    }

    /**
     * update/rename a folder
     * 
     * @return Json
     */
    public function updatefolder() {
        header('Content-type: application/json');
        $user = Auth::user();
        if ($user->company == 0) {
            $folders = Database::table("folders")->where("name", input("foldername"))->where("folder", input("folder"))->first();
        }else{
            $folders = Database::table("folders")->where("name", input("foldername"))->where("folder", input("folder"))->where("company", $user->company)->first();
        }
        if (!empty($folders)) {
            exit(json_encode(responder("error", "Already Exists!", "Folder name '".input("foldername")."' already exists.")));
        }
        Database::table("folders")
                        ->where("id", input("folderid"))
                        ->where("company", $user->company)
                        ->update("name", escape(input("foldername")));
        exit(json_encode(responder("success", "", "","documentsCallback()", false)));
    }

    /**
     * update/rename a file
     * 
     * @return Json
     */
    public function updatefile() {
        header('Content-type: application/json');
        $user = Auth::user();
        if ($user->company == 0) {
            $files = Database::table("files")->where("name", input("filename"))->where("folder", input("folder"))->where("parent", $user->parent)->first();
        }else{
            $files = Database::table("files")->where("name", input("filename"))->where("folder", input("folder"))->where("company", $user->company)->first();
        }
        if (!empty($files)) {
            exit(json_encode(responder("error", "Already Exists!", "File '".input("filename")."' already exists.")));
        }
        Database::table("files")
                        ->where("id", input("fileid"))
                        ->update("name", escape(input("filename")));
        exit(json_encode(responder("success", "", "","documentsCallback()", false)));
    }

    /**
     * Duplicate a file
     * 
     * @return Json
     */
    public function duplicate() {
        header('Content-type: application/json');

        $fileId = input("file");

        if(!empty($fileId)) {
            $filename  = Database::table("files")->where("id", input("file"))->first();

            if($filename == NULL){
                exit(json_encode(responder("error", "Sorry!", "Unable to create duplicate. This file is invalid.")));
            }else{

                $currentpath = realpath(config("app.storage")."files/".$filename->filename);
                $currentHash  = hash_file('sha512' , $currentpath);

                if($currentHash != $filename->latest_hash){
                    exit(json_encode(responder("error", "Sorry!", "Unable to create duplicate. This file is invalid.")));
                }
            }
        }

        $doucmentId = Testclass::duplicate(input("file"));

        $document = Database::table("files")
                                           ->where("id", $doucmentId)
                                           ->first();
        $document_key = $document->document_key;

        $meregqr = new MergeQR();

        $barcode = 1;
        $barcode = $meregqr->GetBarCode();

        $QRpath = config("app.storage")."/qrcode/".$document_key.".png";
        $string = env("APP_URL").url("track/$document_key");
        $meregqr->generateQR($QRpath, $string, $barcode);

        Database::table("files")
                            ->where("id", $doucmentId)
                            ->update(array("barcode"=>$barcode));

        if($document->extension == "docx" || $document->extension == "doc"){
            $tag_id = 3;
        }else if($document->extension == "xls" || $document->extension == "xlsx"){
            $tag_id = 7;
        }else if($document->extension == "ppt" || $document->extension == "pptx"){
            $tag_id = 5;
        }else{
            $tag_id = 1;
        }

        $data = array(
                   "company_id"     => 0,
                   "document_id"    => $document->id,
                   "tag_id"         => $tag_id,
                    );

        Database::table("tagdocuments")->insert($data);

        exit(json_encode(responder("success", "", "","documentsCallback()", false)));
    }

    /**
     * Update folder accessibility view
     * 
     * @return \Pecee\Http\Response
     */
    public function updatefolderaccessview() {
        $user = Auth::user();
        $folder = Database::table("folders")
                                           ->where("company", $user->company)
                                           ->where("id", input("folder"))
                                           ->first();
        $departments = Database::table("departments")->where("company", $user->company)->get();
        if ($folder->accessibility == "Departments") {
            $allowedDepartments = json_decode($folder->departments);
        }else{
            $allowedDepartments = array();
        }
        return view('extras/folderaccess', compact("user", "folder", "departments","allowedDepartments"));
    }

    /**
     * Update folder accessibility 
     * 
     * @return Json
     */
    public function updatefolderaccess() {
        if (input("accessibility") == "Departments") {
            $departments = json_encode($_POST['departments']);
        }else{
            $departments = "";
        }
        $data = array(
                        "accessibility" => input("accessibility"),
                        "departments" => $departments
                    );
        $user = Auth::user();
        $folder = Database::table("folders")
                                           ->where("company", $user->company)
                                           ->where("id", input("folderid"))
                                           ->update($data);
        header('Content-type: application/json');
        exit(json_encode(responder("success", "Alright!", "Folder accessibility updated.","hideSharedModal();")));
    }

    /**
     * Update file accessibility view
     * 
     * @return \Pecee\Http\Response
     */
    public function updatefileaccessview() {
        $user = Auth::user();
        $file = Database::table("files")
                                           ->where("id", input("file"))
                                           ->first();
        $departments = Database::table("departments")->where("company", $user->company)->get();
        if ($file->accessibility == "Departments") {
            $allowedDepartments = json_decode($file->departments);
        }else{
            $allowedDepartments = array();
        }
        return view('extras/fileaccess', compact("user", "file", "departments","allowedDepartments"));
    }

    /**
     * Update file accessibility 
     * 
     * @return Json
     */
    public function updatefileaccess() {
        if (input("accessibility") == "Departments") {
            $departments = json_encode($_POST['departments']);
        }else{
            $departments = "";
        }
        $data = array(
                        "accessibility" => input("accessibility"),
                        "departments" => $departments
                    );
        $user = Auth::user();
        $folder = Database::table("files")
                                           ->where("id", input("fileid"))
                                           ->update($data);
        header('Content-type: application/json');
        exit(json_encode(responder("success", "Alright!", "File accessibility updated.","hideSharedModal();")));
    }

    /**
     * Update folder protection view
     * 
     * @return \Pecee\Http\Response
     */
    public function updatefolderprotectview() {
        $user = Auth::user();
        $folder = Database::table("folders")->where("company", $user->company)->where("id", input("folder"))->first();
        return view('extras/folderprotect', compact("user", "folder"));
    }

    /**
     * Update folder protection
     * 
     * @return Json
     */
    public function updatefolderprotect() {
        header('Content-type: application/json');
        $user = Auth::user();
        $folder = Database::table("folders")->where("company", $user->company)->where("id", input("folderid"))->first();
        if (!empty($folder->password)) {
            if(!hash_compare($folder->password, Auth::password(input("current")))){
                exit(json_encode(responder("error", "Oops!", "Incorrect current password.")));
            }
        }
        if (input("password") == "remove") {
            $password = "";
        }else{
            $password = Auth::password(input("password"));
        }
        Database::table("folders")->where("company", $user->company)->where("id", input("folderid"))->update(array("password" => $password));
        exit(json_encode(responder("success", "Alright!", "Folder protection updated.","reload();")));
    }

    /**
     * delete a folder
     * 
     * @return Json
     */
    public function deletefolder() {
        header('Content-type: application/json');
        $delete = Testclass::deletefolder(input("folder"));
        if ($delete) {
            exit(json_encode(responder("success", "", "","deleted(true);", false)));
        }else{
            exit(json_encode(responder("error", "Oops!", "Delete failed, please try again.","deleted(false);", true, "toastr")));
        }
    }

    /**
     * delete a file
     * 
     * @return Json
     */
    public function deletefile() {
        header('Content-type: application/json');
        $delete = Testclass::deletefile(input("file"));
        if ($delete) {
            if (isset($_POST['source'])) {
                exit(json_encode(responder("success", "", "","redirect('".env("APP_URL")."/documents');", false)));
            }else{
                exit(json_encode(responder("success", "", "","deleted(true);", false)));
            }
        }else{
            exit(json_encode(responder("error", "Oops!", "Delete failed, please try again.","deleted(false);", true, "toastr")));
        }
    }


    /**
     * delete multiple items
     * 
     * @return Json
     */
    public function delete() {
        header('Content-type: application/json');
        // echo "<pre>"; print_r($_POST['data']); die();
        foreach ($_POST['data'] as $data) {
            if ($data["type"] == "folder") {
                Testclass::deletefolder($data["itemid"]);
            }else{
                Testclass::deletefile($data["itemid"]);
            }
        }
        exit(json_encode(responder("success", "", "","deleted(true);", false)));
    }

    /**
     * Upload a file
     * 
     * @return Json
     */
    public function uploadfile() {
        header('Content-type: application/json');
        $user = Auth::user();
        
        $meregqr = new MergeQR();

        if($user->company){
            $parentID = 0;
        }else{
            $parentID = $user->parent;
        }

        $barcode = 1;
        
            $barcode = $meregqr->GetBarCode();
        

        $clientIP = Testclass::get_client_ip();
        $data = array(
                  "company"     => $user->company,
                  "parent"      => $parentID,
                  "uploaded_by" => $user->id,
                  "name"        => input("name"),
                  "folder"      => input("folder"),
                  "file"        => $_FILES['file'],
                  "is_template" => "No",
                  "source"      => "form",
                  "document_key"=> Str::random(32),
                  "barcode"     => $barcode,
                  "activity"    => 'File uploaded by <span class="text-primary">'.escape($user->fname.' '.$user->lname).'['.$clientIP.']</span>.'
                    );
       
        $upload = Testclass::upload($data);

        $document_key = $data['document_key'];

        $QRpath = config("app.storage")."qrcode/".$document_key.".png";
        $string = env("APP_URL").url("track/$document_key");
        $meregqr->generateQR($QRpath, $string, $barcode);

        $document = Database::table("files")->where("document_key", $document_key)->first();

        if($document->extension == "docx" || $document->extension == "doc"){
            $tag_id = 3;
        }else if($document->extension == "xls" || $document->extension == "xlsx"){
            $tag_id = 7;
        }else if($document->extension == "ppt" || $document->extension == "pptx"){
            $tag_id = 5;
        }else{
            $tag_id = 1;
        }

        $data = array(
                   "company_id"     => 0,
                   "document_id"    => $document->id,
                   "tag_id"         => $tag_id,
                    );

        Database::table("tagdocuments")->insert($data);

        if ($upload['status'] == "success") {
            exit(json_encode(responder("success", "", "","documentsCallback()", false)));
        }else{
            exit(json_encode(responder("error", "Oops!", $upload['message'])));
        }
    }

    /**
     * Save file imported from Dropbox
     * 
     * @return Json
     */
    public function dropboximport() {
        header('Content-type: application/json');
        $user = Auth::user();
        $clientIP = Testclass::get_client_ip();
        $data = array(
                        "company" => $user->company,
                        "uploaded_by" => $user->id,
                        "name" => input("name"),
                        "folder" => input("folder"),
                        "file" => input("url"),
                        "is_template" => "No",
                        "source" => "url",
                        "document_key" => Str::random(32),
                        "activity" => 'File Imported from Dropbox by <span class="text-primary">'.escape($user->fname.' '.$user->lname).'['.$clientIP.']</span>.'
                    );
        $upload = Testclass::upload($data);
        if ($upload['status'] == "success") {
            exit(json_encode(responder("success", "", "","documentsCallback()", false)));
        }else{
            exit(json_encode(responder("error", "Oops!", $upload['message'])));
        }
        exit(json_encode(responder("success", "", "","documentsCallback()", false)));
    }

    /**
     * Save file imported from Google Drive
     * 
     * @return Json
     */
    public function googledriveimport() {
        $fileName = Str::random(32).".pdf";
        $outputFile = config("app.storage")."/files/".$fileName;
        putenv('GOOGLE_APPLICATION_CREDENTIALS=uploads/credentials/keys.json');
        $client = new Google_Client();
        $client->addScope(Google_Service_Drive::DRIVE);
        $client->useApplicationDefaultCredentials();
        $service = new Google_Service_Drive($client);
        try {
            $content = $service->files->export(input('fileId'), 'application/pdf', array("alt" => "media"));
        }
        catch(\Exception $e) {
            header('Content-type: application/json');
            exit(json_encode(responder("error", "Oops!", $e->getMessage())));
        }
        $headers = $content->getHeaders();
        foreach ($headers as $name => $values) {
            header($name . ': ' . implode(', ', $values));
        }
        $f = fopen($outputFile, 'w');
        fwrite($f, $content->getBody());
        fclose($f);
        $user = Auth::user();
        $clientIP = Testclass::get_client_ip();
        $data = array(
                        "company" => $user->company,
                        "uploaded_by" => $user->id,
                        "name" => input("name"),
                        "folder" => input("folder"),
                        "file" => $fileName,
                        "is_template" => "No",
                        "source" => "googledrive",
                        "document_key" => Str::random(32),
                        "size" => round(filesize($outputFile) / 1000),
                        "activity" => 'File Imported from Google Drive by <span class="text-primary">'.escape($user->fname.' '.$user->lname).'['.$clientIP.']</span>.'
                    );
        $upload = Testclass::upload($data);
        header('Content-type: application/json');
        if ($upload['status'] == "success") {
            exit(json_encode(responder("success", "", "","documentsCallback()", false)));
        }else{
            exit(json_encode(responder("error", "Oops!", $upload['message'])));
        }
        exit(json_encode(responder("success", "", "","documentsCallback()", false)));
    }


    /**
     * Restore file version
     * 
     * @return Json
     */
    public function restore() {
        $user = Auth::user();
        $document = Database::table("files")->where("id", input("file"))->first();
        Testclass::deletefile($document->filename, "original");
        copy(config("app.storage")."copies/".$document->filename, config("app.storage")."files/".$document->filename);
        $data = array( "status" => "Unsigned", "sign_reason" => "", "editted" => "No" );
        Database::table("files")->where("document_key", $document->document_key)->update($data);
        $clientIP = Testclass::get_client_ip();
        $activity = 'File restored to orginal version by <span class="text-primary">'.escape($user->fname.' '.$user->lname).'['.$clientIP.']</span>';
        Testclass::keephistory($document->document_key, $activity);
        header('Content-type: application/json');
        exit(json_encode(responder("success", "Restored!", "Original version successfully restored.","reload()")));
    }

    /**
     * Replace a file
     * 
     * @return Json
     */
    public function replace() {
        header('Content-type: application/json');
        $user = Auth::user();
        $document_key = input("document_key");
        $document = Database::table("files")->where("document_key", $document_key)->first();
        if(env("ALLOW_NON_PDF") == "Enabled"){
            $allowedExtensions = "pdf, doc, docx, ppt, pptx, xls, xlsx";
        }else{
            $allowedExtensions = "pdf";
        }
        $upload = File::upload(
            $_FILES['file'], 
            "files",
            array(
                "allowedExtesions" => $allowedExtensions,
            )
        );
        if ($upload['status'] == "success") {
            Testclass::deletefile($document->filename, true);
            Testclass::keepcopy($upload['info']['name']);
            $data = array(
                            "status" => "Unsigned",
                            "editted" => "No",
                            "sign_reason" => "",
                            "filename" => $upload['info']['name'],
                            "size" => $upload['info']['size'],
                            "extension" => $upload['info']['extension']
                        );
            Database::table("files")->where("document_key", $document_key)->update($data);
            $clientIP = Testclass::get_client_ip();
            $activity = 'File replaced with a new one by <span class="text-primary">'.escape($user->fname.' '.$user->lname).'['.$clientIP.']</span>.';
            Testclass::keephistory($document_key, $activity);
            exit(json_encode(responder("success", "Replaced!", "File successfully replaced.","reload()")));
        }else{
            exit(json_encode(responder("error", "Oops!", $upload['message'])));
        }
    }

    /**
     * Protect a pdf file
     * 
     * @return Json
     */
    public function protect() {
        header('Content-type: application/json');
        if(!isset($_POST['permission']) && !isset($_POST['setPassword'])){
            exit(json_encode(responder("warning", "Oops!", "No protection mode selected..")));
        }
        if(isset($_POST['permission'])){
            $permission = $_POST['permission'];
        }else{
            $permission = array();
        }
        if(isset($_POST['setPassword'])){
            $userpassword = $_POST['userpassword'];
            $ownerpassword = $_POST['ownerpassword'];
        }else{
            $userpassword = null;
            $ownerpassword = null;
        }
        $protected = Testclass::protect($permission, $userpassword, $ownerpassword, input("document_key"));
        if ($protected) {
            exit(json_encode(responder("success", "Alright!", "Document protection activated.","reload()")));
        }else{
            exit(json_encode(responder("error", "Oops!", "Testclass could not modify this file, the file could be protected.")));
        }
    }

    /**
     * Send file
     * 
     * @return Json
     */
    public function send() {
        header('Content-type: application/json');
        $user = Auth::user();
        $document_key = input("document_key");
        $document = Database::table("files")->where("document_key", $document_key)->first();
        $emails = explode(",", input("receivers"));
        foreach ($emails as $email) {
            $send = Mail::send(
                $email,
                "You have received a document from ".$user->fname." ".$user->lname,
                array(
                    "message" => "Hello there,<br><br>You have received a file from ".$user->fname." ".$user->lname.".<br><strong>Message:</strong> ".input("message")."<br><br>Cheers!<br>".env("APP_NAME")." Team"
                ),
                "basic",
                null,
                array($document->name.'.'.$document->extension => config("app.storage")."files/".$document->filename)
            );

            if (!$send) {
                exit(json_encode(responder("error", "Oops!", $send->ErrorInfo)));
            }
        }
        exit(json_encode(responder("success", "Sent!", "File successfully sent.","reload()")));
    }

    /**
     * Convert file to PDF
     * 
     * @return Json
     */
    public function convert() {
        header('Content-type: application/json');
        $convert = Testclass::convert(input("document_key"));
        if ($convert['status'] == "success") {
            exit(json_encode(responder("success", "Converted!", "File successfully converted.","reload()")));
        }else{
            exit(json_encode(responder("error", "Failed!", $convert['message'])));
        }
        
    }

    /**
     * Sign & Edit Document
     * 
     * @return Json
     */
    public function sign() {
        $sign = Testclass::sign(input("document_key"), input("actions"), input("docWidth"), input("signing_key"));
        header('Content-type: application/json');
        if ($sign) {
            exit(json_encode(responder("success", "Alright!", "Document successfully saved.","reload()")));
        }else{
            exit(json_encode(responder("error", "Oops!", "Something went wrong, please try again.")));
        }
        
    }

    /**
     * Update Document Permissions
     * 
     * @return Json
     */
    public function permissions() {
        header('Content-type: application/json');
        Database::table("files")->where("id", input("document"))->update(array("public_permissions" => input("public_permissions")));
        exit(json_encode(responder("success", "Alright!", "Document permissions updated.","","true","toastr")));
    }

    public function tag(){
        header('Content-type: application/json');
        $tags = $_POST['tags'];
        $document_key = input('document_key');
        $csrf_token = input('csrf-token');

        if(!count($tags)){
          exit(json_encode(responder("error", "Oops!", "At leaset one tag selected")));
        }

        $document = Database::table("files")->where("document_key", $document_key)->first();

        $document_id = $document->id;

        if(Auth::user()->company == 0){
            $data['company_id'] = Auth::user()->parent;
        }else{
            $data['company_id'] = Auth::user()->company;
        }

        $data['document_id'] = $document_id;

        Database::table("tagdocuments")->where("company_id", $data['company_id'])->where("document_id", $document_id)->delete();

        foreach($tags as $tag){
          $data['tag_id'] = $tag;

          Database::table("tagdocuments")->insert($data);
        }
        exit(json_encode(responder("success", "Update!", "Tag successfully updated.","reload()")));
    
    }

    public function movefilefolder(){
        header('Content-type: application/json');

        $post = $_POST;

        if(empty($post['move_in'])){
            exit(json_encode(responder("error", "Oops!", "Something went wrong. Please try again.","reload()")));
        }

        if($post['move_type'] === 'file'){
            Database::table("files")->where("id", $post["move_to"])->update("folder", $post["move_in"]);
        }elseif($post['move_type'] === 'folder'){
            Database::table("folders")->where("id", $post["move_to"])->update("folder", $post["move_in"]);
        }else{
            exit(json_encode(responder("error", "Oops!", "Something went wrong. Please try again.","reload()")));
        }

        exit(json_encode(responder("success", "Done!", "Move successfully complete.","reload()")));
    }

    public function movefilefolderview() {
        $post = $_POST;

        $move_to = $post['id'];
        $move_type = $post['type'];
        $parent = $post['parent'];

        $user = Auth::user();

        $folders = Database::table("folders")
                                           ->where("created_by", $user->id)
                                           ->get();

        if($post['type'] == 'file'){
            return view('extras/movefilefolder', compact("user", "folders", "move_to", "move_type", "parent"));
        }

        $returnFolders = $this->sortFolders($folders, (array)$post['id']);

        unset($folders);
        $folders = $returnFolders;

        return view('extras/movefilefolder', compact("user", "folders", "move_to", "move_type", "parent"));
    }

    private function sortFolders($folders, $parentID){
        $folders = array_values($folders);
        $re = array();
        $i = -1;
        foreach ($folders as $folder) {
            $i++;
            if(in_array ( $folder->id, $parentID)) {
                unset($folders[$i]);
            }elseif(in_array ( $folder->folder, $parentID)){
                unset($folders[$i]);
                $re[] = $folder->id;
            }
        }

        if(!empty($re)){
            $folders = $this->sortFolders($folders, $re);
        }

        return $folders;
    }

    public function getFolders($id){
        $data = $this->getChild($id);
        if(count($data)>0){
            foreach($data as $child){
                $childs[$child->id] = $this->getFolders($child->id);
            }
            return $childs;
        }else{
            return $childs;
        }
    }

    public function getChild($id){
        $childs = Database::table("folders")
                       ->where("folder", $id)
                       ->get();
        return $childs;
    }

    public function chageView(){
        $user = Auth::user();
        $data = array(
                    "doc_view" => $_POST['view'],
                    "user_id"  => $user->id,
                    );
        Database::table("document_view")->where("user_id", $user->id)->delete();
        Database::table("document_view")->insert($data);
    }

    public function CreateFile(){
        
        $post = $_POST;
        header('Content-type: application/json');
        
        $meregqr = new MergeQR();
        $barcode = 1;
        $barcode = $meregqr->GetBarCode();

        if (input("head")=='1') {
            $storage = dirname(__DIR__).'/TCPDF/examples/images';
            $fileName = $_FILES['file']['name'];
            $fileName = Str::random(32).$fileName;
            $outputFile = $storage."/".$fileName;
            move_uploaded_file($_FILES['file']["tmp_name"], $outputFile);

        $doucmentId = Testclass::create(input("fname"), input("folder"),input("head"),input("number"), $barcode, input("hTitle"), input("hString"), $fileName);
        }else{
           $doucmentId = Testclass::create(input("fname"), input("folder"),input("head"),input("number"), $barcode); 
        }

        $data = array(
                   "company_id"     => 0,
                   "document_id"    => $doucmentId,
                   "tag_id"         => 1,
                    );

        Database::table("tagdocuments")->insert($data);

        $document = Database::table("files")
                                           ->where("id", $doucmentId)
                                           ->first();
        $document_key = $document->document_key;

        

        $QRpath = config("app.storage")."/qrcode/".$document_key.".png";
        $string = env("APP_URL").url("track/$document_key");
        $meregqr->generateQR($QRpath, $string, $barcode);

        exit(json_encode(responder("success", "", "","documentsCallback()", false)));
    }

    public function archive() {
        
        Database::table("files")->where("id", input('file'))->update("is_archive", (int)1);
        header('Content-type: application/json');
        exit(json_encode(responder("success", "", "File archive.","reload();", true)));
    }

    public function archivefolder() {
        Database::table("folders")->where("id", input('folder'))->update("is_archive", (int)1);
        header('Content-type: application/json');
        exit(json_encode(responder("success", "", "Folder archive.","reload();", true)));
    }




     public function fetchdata() {

        $user = Auth::user();
        if(isset($_COOKIE['item-count'])){
            $limit = $_COOKIE['item-count'];
        }else{
            $limit = 20;
        }

        if(input('sort-column') != ""){
            $sortColumn = input('sort-column');
        }else{
            $sortColumn = "name";
        }

        if(input('sort-order') != ''){
            $sortOrder .= input('sort-order');
            $sortOrderClass = input('sort-order');
        }else{
            $sortOrder .= "ASC";
            $sortOrderClass = "ASC";
        }

        $orderBy = " AND `is_archive` = 1 GROUP BY `document_key`  ORDER BY ".$sortColumn." ".$sortOrder;

        if(isset($_POST['page']) && $_POST['page'] != ''){
            $page = $_POST['page'];
            $startFrom = ($page - 1) * $limit;
        }else{
            $page = 1;
            $startFrom = 0;
        }
            $endPoint = $startFrom+$limit;

        if(!isset($_POST['c'])){
           $c = 0;
           $cp = $page;
        }else{
           $c = $_POST['c'];
           $cp = $_POST['cp'];
        }

        $thisFolder = Database::table("folders")->where("id", input("folder"))->first();
        $hiddenFiles = json_decode($user->hiddenfiles);
        $folders = $documents = array();
        if (!empty($thisFolder->password)) {
            if (empty(input("password"))) {
                $protected = true;
                $incorrect = false;
                return view('extras/documents', compact("user", "protected", "incorrect"));
            }else{
                if(!hash_compare($thisFolder->password, Auth::password(input("password")))){
                    $incorrect = $protected = true;
                    return view('extras/documents', compact("user", "protected", "incorrect"));
                }
            }
        }
        if (empty($_POST['type']) || $_POST['type'] == "folders") {
            $foldersQuery = "SELECT * FROM folders ";

            if (!empty(input("search"))) {
                $foldersQuery .= " WHERE name LIKE '%".input("search")."%'"."  AND created_by = ".$user->id;
            }else{
                $foldersQuery .= " WHERE folder = ".input("folder")."  AND created_by = ".$user->id;
            }

            if ($user->role == "user" && $folder->created_by != $user->id) {
                $foldersQuery .= " AND created_by = ".$user->id;
            }

            $foldersQuery .= $orderBy;

            $foldersQuery = str_replace("document_key","id",$foldersQuery);
            $foldersCount = Database::table("folders")->queryFire($foldersQuery);

        }
        if (empty($_POST['type']) || $_POST['type'] == "files") {
            $sharedFileKeys = array();
            $requestsData = Database::table("requests")->where("receiver", $user->id)->orWhere("email", $user->email)->orderBy("id", false)->get();

            $documentsQueryStatus = $documentsQueryExtension = $documentsQueryUser = $documentsQueryHiddenFiles = '';
            if(isset($hiddenFiles) and $hiddenFiles != null){
                if(input('search') != ''){
                    $documentsQueryHiddenFiles = " AND ( a.id NOT IN ('".implode("', '", @$hiddenFiles)."')";
                }else{
                    $documentsQueryHiddenFiles = " AND ( id NOT IN ('".implode("', '", @$hiddenFiles)."')";
                }
            }else{
                if(input('search') != ''){
                    $documentsQueryHiddenFiles = " AND ( a.id NOT IN ('')";
                }else{
                    $documentsQueryHiddenFiles = " AND ( id NOT IN ('')";
                }
            }
            $documentsQueryHiddenFiles .= " OR document_key IN ('".implode("', '", $sharedFileKeys)."') )";

            if (!empty($_POST['status'])) {
                $documentsQueryStatus = " AND status = ".$_POST['status']." ";
            }
            if (!empty($_POST['extension'])) {
                $documentsQueryExtension = " AND ( extension = '".$_POST['extension']."' OR extension = '".$_POST['extension']."x' )"." ";
            }
            if ($user->role == "user" && $document->uploaded_by != $user->id) {
                $documentsQueryUser = " AND uploaded_by = ".$user->id." ";
            }

            if (!empty(input("search"))) {
                $search = input("search");
                $where = 'a.`uploaded_by` = "'.$user->id.'"';

                $documentsQuery = (string)'SELECT a.* FROM `files` a LEFT JOIN `tagdocuments` c on c.`document_id` = a.`id` LEFT JOIN `tagmaster` b on b.`tag_id` = c.`tag_id` WHERE '.$where.' '.$documentsQueryStatus.$documentsQueryExtension.$documentsQueryUser.$documentsQueryHiddenFiles.' AND (b.`tagname` LIKE "%'.$search.'%" OR a.`name` LIKE "%'.$search.'%") '. $orderBy;
                
            }else{
                if($user->company==0){
                    $documentsQuery = "SELECT * FROM files WHERE uploaded_by = ".$user->id." AND folder = ".input("folder"). " AND is_template = 'No'  ".$orderBy;
                }else{
                    $documentsQuery = "SELECT * FROM files WHERE uploaded_by = ".$user->id." AND folder = ".input("folder"). " AND is_template = 'No' " .$documentsQueryStatus. $documentsQueryExtension . $documentsQueryUser . $documentsQueryHiddenFiles . $orderBy;
                }
            }

            $documentsCount = Database::table("files")->queryFire($documentsQuery);
        }
        
        $fileCount      = count($documentsCount);
        $folderCount    = count($foldersCount);

        if($folderCount >= $endPoint){
            $foldersQuery .= " LIMIT ".$startFrom." , ".$limit;

            $foldersQuery = str_replace("document_key","id",$foldersQuery);
            $folders = Database::table("folders")->queryFire($foldersQuery);
            
            $documents = array();
        }elseif($folderCount < $endPoint && $folderCount > $startFrom) {
            $newLimit = $folderCount-$startFrom;
            $foldersQuery .= " LIMIT ".$startFrom." , ".$newLimit;

            $foldersQuery = str_replace("document_key","id",$foldersQuery);
            $folders = Database::table("folders")->queryFire($foldersQuery);

            $documentsQuery .= " LIMIT 0 , ";
            $documentsQuery .= ($limit-$newLimit);

            $documents = Database::table("files")->queryFire($documentsQuery);
            $c = $limit-$newLimit;
            $cp = $page;
        }else{
            $folders = array();

            if($c == 0 && $page != 1){
                $s = ($page - 1)*$limit;
            }else{
                if($folderCount == 0){
                    $s = ($page-1)*$limit;
                }else{
                    $s = ($page - $cp)*$c;    
                    
                    $a = ($page * $limit) - $limit;
                    $b = (($cp*$limit) - $c);

                    $s = $a-$b;
                }
            }

            $documentsQuery .= " LIMIT ".$s." , ".$limit;

            $documents = Database::table("files")->queryFire($documentsQuery);
        }
            $total = $folderCount+$fileCount;
            $html = '';
            if($total > 0){

                if($limit > $total){
                    $quotient = 0;
                    $remainder = 0;
                }else{
                    $remainder = $total % $limit;
                    $quotient = ($total - $remainder) / $limit;;
                }

                if($remainder != 0) {
                    $totalPage = $quotient + 1;
                }else{
                    $totalPage = $quotient;
                }

                if($sortOrder == 'ASC'){
                    $rSortOrder = 'DESC';
                }else{
                    $rSortOrder = 'ASC';
                }

                $sortOrder = "'".$sortOrder."'";
                $sortColumn2 = $sortColumn;
                $sortColumn = "'".$sortColumn."'";
                $rSortOrder = "'".$rSortOrder."'";

                $html = '';
                $currentPage = 1;
                for ($p=1; $p <= $totalPage; $p++) {
                    if($p == $page){$cls="active"; $currentPage = $p; }else{$cls='';}
                    $html .= '<li class="pageLI '.$cls.'"><a onclick="pages('.$p.', event, this, '.$c.', '.$cp.', '.$sortOrder.', '.$sortColumn.')" href1="#"><span>'.$p.'<span></a></li>';
                }

                if($currentPage == 1 ){
                    $previous = '<li class="pageLI disabled"><a class="previous disabled"><span>Previous<span></a></li>';
                }else{
                    $previous = '<li class="pageLI"><a onclick="pages('.($currentPage-1).', event, this, '.$c.', '.$cp.', '.$sortOrder.', '.$sortColumn.')" href1=""><span>Previous<span></a></li>';
                }

                if($currentPage == $totalPage || $totalPage == 0){
                    $next = '<li class="pageLI disabled"><a><span>Next<span></a></li>';
                }else{
                    $next = '<li class="pageLI"><a onclick="pages('.($currentPage+1).', event, this, '.$c.', '.$cp.', '.$sortOrder.', '.$sortColumn.')" href1=""><span>Next<span></a></li>';
                }
                if($html === ''){
                    $html .= '<li class="pageLI active"><a onclick="pages(1, event, this, '.$c.', '.$cp.', '.$sortOrder.', '.$sortColumn.')" href1=""><span>'.$p.'<span></a></li>';
                }

                if($totalPage == 1 || $totalPage == 0){
                    $hidePagination = 'style = "display: none;"';
                }else{
                    $hidePagination = "";
                }
                $page_html = '<div class="col-md-12"><ul class="pagination pull-right" '.$hidePagination.'>'.$previous.$html.$next.'</ul></div>';
            }
                        
            $doc_view = Database::table("document_view")->where("user_id", $user->id)->first();

            if($doc_view === NULL){
                $view_style = 0;
            }else{
                $view_style = $doc_view->doc_view;
            }

            if($currentPage == 1) {
                $srNo = 0 + 1;
            } else {
                $srNo = (($currentPage - 1)*$limit) + 1;
            }

            $res = array(
                        'html' => view('extras/documentslist', compact("user", "folders", "documents", "view_style", "sortOrder", "sortColumn2", "rSortOrder", "srNo", "sortOrderClass")),
                        'pagination'=> $page_html,
                        'view_style'=> $view_style,

                    );

            exit(json_encode($res));

        return view('extras/documentslist', compact("user", "folders", "documents"));
    }


}

