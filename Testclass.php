<?php 
namespace Src;

require_once 'TCPDF/tcpdf.php';
require_once 'TCPDF/tcpdi.php';
require_once 'TCPDF/include/tcpdf_fonts.php';
use TCPDF_FONTS;
use TCPDF;
use TCPDI;
use Src\Str;
use Src\File;
use Src\Auth;
use Src\Database;
use \CloudConvert\Api;
use TCPDF2DBarcode;
use Src\MergeQR;

class PDF extends TCPDI {
    var $_tplIdx;
    var $numPages;

    function Header() {}

    function Footer() {}

}

class Testclass {
    
    /**
     * Upload file
     * 
     * @param   array $data
     * @return  true
     */
    public static function upload($data) {
        $user = Auth::user();
        // get usage data
        if ($user->role == "user") {
            $fileUsage = Database::table("files")->where("uploaded_by" , $user->id)->count("id", "files")[0]->files;
            $diskUsage = Database::table("files")->where("uploaded_by" , $user->id)->sum("id", "size")[0]->size / 1000;
            // check file usage limits
            if ($fileUsage > env("PERSONAL_FILE_LIMIT")) {
                return responder("error", "Limit Exceeded!", "You have exceeded your limit of ".env("PERSONAL_FILE_LIMIT")." files.");
            }
            // check disk usage limits
            if ($diskUsage > env("PERSONAL_DISK_LIMIT")) {
                return responder("error", "Limit Exceeded!", "You have exceeded your limit of ".env("PERSONAL_DISK_LIMIT")." MBs.");
            }
        }else{
            $fileUsage = Database::table("files")->where("company" , $user->company)->count("id", "files")[0]->files;
            $diskUsage = Database::table("files")->where("company" , $user->company)->sum("id", "size")[0]->size / 1000;
            // check file usage limits
            if ($fileUsage > env("BUSINESS_FILE_LIMIT")) {
                return responder("error", "Limit Exceeded!", "You have exceeded your limit of ".env("BUSINESS_FILE_LIMIT")." files.");
            }
            // check disk usage limits
            if ($diskUsage > env("BUSINESS_DISK_LIMIT")) {
                return responder("error", "Limit Exceeded!", "You have exceeded your limit of ".env("BUSINESS_DISK_LIMIT")." MBs.");
            }
        }
        if ($user->company == 0) {
            $files = Database::table("files")->where("name", $data["name"])->where("folder", $data["folder"])->where("parent", $user->parent)->first();
        }else{
            $files = Database::table("files")->where("name", $data["name"])->where("folder", $data["folder"])->where("company", $user->company)->first();
        }
        if (!empty($files) && $data["source"] == "form") {
            return responder("error", "Already Exists!", "File name '".$data["name"]."' already exists.");
        }
        if(env("ALLOW_NON_PDF") == "Enabled"){
            $allowedExtensions = "pdf, doc, docx, ppt, pptx, xls, xlsx, jpg, jpeg, png, txt";
        }else{
            $allowedExtensions = "pdf";
        }
        if ($data['source'] == "googledrive") {
            $upload = array(
                                "status" => "success",
                                "info" => array(
                                                    "name" => $data['file'],
                                                    "size" => $data['size'],
                                                    "extension" => "pdf"
                                                )
                            );
        }else{
            $upload = File::upload(
                $data['file'], 
                "files",
                array(
                    "source" => $data['source'],
                    "allowedExtensions" => $allowedExtensions
                )
            );
        }

        if ($upload['status'] == "success") {
            self::keepcopy($upload['info']['name']);
            $data["filename"] = $upload['info']['name'];
            $data["size"] = $upload['info']['size'];
            $data["extension"] = $upload['info']['extension'];
            $activity = $data['activity'];
            unset($data['file'], $data['source'], $data['activity']);
            
            $filePath = 'file://'.realpath(config("app.storage")."files/".$data['filename']);
            $data['int_file_hash']  = hash_file('sha512' , $filePath);
            $data['latest_hash']    = $data['int_file_hash'];
            $upload['document_key'] = $data['document_key'];

            Database::table("files")->insert($data);
            $documentId = Database::table("files")->insertId();
            $document = Database::table("files")->where("id", $documentId)->get("document_key");

            self::keephistory($document[0]->document_key, $activity);
            $activity2 = 'Initial hash value is generated.';

            self::keephistory($document[0]->document_key, $activity2);
            return responder("success", "Upload Complete", "File successfully uploaded.");
        }else{
            return responder("error", "Oops!", $upload['message']);
        }
    }
    
    /**
     * Duplicate file
     * 
     * @param   int $data
     * @return  true
     */
    public static function duplicate($file, $duplicateName = '') {
        $document = Database::table("files")->where("id", $file)->first();
        $user = Auth::user();
        $fileName = Str::random(32).".".$document->extension;
        copy(config("app.storage")."files/".$document->filename, config("app.storage")."files/".$fileName);
        self::keepcopy($fileName);
        $clientIP = self::get_client_ip();
        if(empty($duplicateName)){ $duplicateName = $document->name." (Copy)"; }
        $activity = 'File Duplicated from '.escape($document->name).' by <span class="text-primary">'.escape($user->fname.' '.$user->lname).'['.$clientIP.']</span>.';

        $newPath = config("app.storage")."files/".$fileName;
        $hashValue = hash_file('sha512' , $newPath);

        $data = array(
                        "company"       => $user->company,
                        "parent"       => $user->parent,
                        "uploaded_by"   => $user->id,
                        "name"          => $duplicateName,
                        "folder"        => $document->folder,
                        "filename"      => $fileName,
                        "extension"     => $document->extension,
                        "size"          => $document->size,
                        "status"        => $document->status,
                        "is_template"   => $document->is_template,
                        "document_key"  => Str::random(32),
                        "int_file_hash" => $hashValue,
                        "latest_hash"   => $hashValue,
                    );
        Database::table("files")->insert($data);
        $documentId = Database::table("files")->insertId();
        $document = Database::table("files")->where("id", $documentId)->get("document_key");
        self::keephistory($document[0]->document_key, $activity);
        return $documentId;
    }
    
    /**
     * Copy file
     * 
     * @param   string $filename
     * @return  true
     */
    public static function keepcopy($filename) {
        copy(config("app.storage")."files/".$filename, config("app.storage")."copies/".$filename);
        return true;
    }
    
    /**
     * Copy file
     * 
     * @param   string $filename
     * @return  true
     */
    public static function renamecopy($fileName, $newName) {
        rename(config("app.storage")."copies/".$fileName, config("app.storage")."copies/".$newName);
        return true;
    }
    
    /**
     * Delete a folder
     * 
     * @param   string|int $folderId
     * @return  true
     */
    public static function deletefolder($folderId) {
        $foldersToDelete = $filesToDelete = array();
        $user = Auth::user();
        $thisFolder = Database::table("folders")->where("id", $folderId)->first();
        if ($user->company != $thisFolder->company) {
            return false;
        }
        $folders = Database::table("folders")
                         ->where("folder", $folderId)
                         ->get();
        foreach ($folders as $folder) {
            $foldersToDelete[] = $folder->id;
            $folderFiles = Database::table("files")->where("folder", $folder->id)->get();
            foreach ($folderFiles as $file) {
                self::deletefile($file->filename, true);
            }
            self::deletefolder($folder->id);
        }
        $folderFiles = Database::table("files")->where("folder", $folderId)->get();
        foreach ($folderFiles as $file) {
            self::deletefile($file->filename, true);
        }
        Database::table("folders")->where("id", $folderId)->delete();
        return true;
    }
    
    /**
     * Delete a file
     * 
     * @param   int $fileId
     * @return  true
     */
    public static function deletefile($fileId, $actualFile = false) {
        if (!$actualFile) {
            $user = Auth::user();
            $thisFile = Database::table("files")->where("id", $fileId)->first();

                File::delete($thisFile->filename, "files");
                File::delete($thisFile->filename, "copies");
                File::delete($qrfile, "qrcode");
                Database::table("files")->where("id", $thisFile->id)->delete();
            }
             return true;
    }

      
    
    /**
     * Record file history
     * 
     * @param   string $document_key
     * @param   string $activity
     * @param   string $type
     * @return  true
     */
    public static function keephistory($document_key, $activity, $type = "default") {
        $document = Database::table("files")->where("document_key", $document_key)->first();
        // $clientIP = self::get_client_ip();
        // $activity .= ' IP: '.$clientIP;
        $time = self::currentUTC();

        Database::table("history")->insert(array("company" => $document->company, "file" => $document_key, "activity" => $activity, "type" => $type, "time_" => $time));
        return true;
    }

    function get_client_ip() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

    public function currentUTC($timeFormat = 'Y-m-d H:i:s'){
        $UTCTimeZone = new \DateTimeZone("UTC");
        $datetime = new \DateTime('now', $UTCTimeZone);
        return $datetime->format($timeFormat);
    }

    public function userTimeConvert($userTimeZone, $time, $timeFormat = 'Y-m-d H:i:s'){
        $newDateTime = new \DateTime($time, new \DateTimeZone("UTC"));
        $newDateTime->setTimezone(new \DateTimeZone($userTimeZone));
        return $newDateTime->format('Y-m-d H:i:s');
    }
    
    /**
     * Save notifications
     * 
     * @param   int $user
     * @param   string $notification
     * @param   string $type
     * @return  true
     */
    public static function notification($user, $notification, $type = "warning", $document_key) {
        Database::table("notifications")->insert(array("user" => $user, "message" => $notification, "type" => $type, "document_key"=>$document_key));
        return true;
    }
    
    /**
     * Convert file to PDF
     * 
     * @param   string $document_key
     * @return  array
     */
    public static function convert($document_key) {
        $user = Auth::user();
        $document = Database::table("files")->where("document_key", $document_key)->first();
        $outputName = Str::random(32).".pdf";
        if (env('USE_CLOUD_CONVERT') == "Enabled" && !empty(env('CLOUDCONVERT_APP_KEY'))) {
            $api = new Api(env('CLOUDCONVERT_APP_KEY'));
            try {
                $api->convert([
                        'inputformat' => $document->extension,
                        'outputformat' => 'pdf',
                        'input' => 'upload',
                        'file' => fopen(config("app.storage").'/files/'.$document->filename, 'r'),
                    ])
                    ->wait()
                    ->download(config("app.storage").'/files/'.$outputName);
            } catch (\CloudConvert\Exceptions\ApiBadRequestException $e) {
                return responder("error", "Failed!", $e->getMessage());
            } catch (\CloudConvert\Exceptions\ApiConversionFailedException $e) {
                return responder("error", "Failed!", $e->getMessage());
            }  catch (\CloudConvert\Exceptions\ApiTemporaryUnavailableException $e) {
                return responder("error", "Failed!", $e->getMessage());
            } catch (\Exception $e) {
                return responder("error", "Failed!", $e->getMessage());
            }
        }else if(env('USE_CLOUD_CONVERT') == "Disabled"){    
            return responder("error", "Failed!", "Cloud Convert is not enabled, please enable on system settings page.");
        }else{    
            return responder("error", "Failed!", "Your Cloud Convert API Key is empty.");
        }
        self::deletefile($document->filename, true);
        self::keepcopy($outputName);
        $data = array(
                        "filename" => $outputName,
                        "size" => round(filesize(config("app.storage")."/files/".$outputName) / 1000),
                        "extension" => "pdf"
                    );
        Database::table("files")->where("document_key", $document_key)->update($data);
        $clientIP = self::get_client_ip();
        $activity = 'File converted to PDF by <span class="text-primary">'.escape($user->fname.' '.$user->lname).'['.$clientIP.']</span>.';
        self::keephistory($document_key, $activity);
        return responder("success", "Complete!", "Conversion successfully completed.");
    }
    
    /**
     * Check file orientation
     * 
     * @param   float $width
     * @param   float $height
     * @return  string
     */
    public static function orientation($width, $height) {
        if ($width > $height) {
            return "L";
        }else{
            return "P";
        }
    }
    
    /**
     * Protect file
     * 
     * @param   string $document_key
     * @return  true
     */
    public static function protect($permission, $userpassword, $ownerpassword, $document_key) {
        $user = Auth::user();
        $document = Database::table("files")->where("document_key", $document_key)->first();
        $pdf = new PDF();
        $inputPath = config("app.storage")."/files/".$document->filename;
        $outputName = Str::random(32).".pdf";
        $outputPath = config("app.storage")."/files/". $outputName;
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        if (env("PKI_STATUS") == "Enabled") {
            $certificate = 'file://'.realpath(config("app.storage").'/credentials/tcpdf.crt');
            $reason = $document->sign_reason.' • Digital Signature | '.$user->fname.' '.$user->lname.', '.self::ipaddress().','.date("F j, Y H:i");
            $info = array( 'Name' => $userName,  'Location' => env("APP_URL"), 'Reason' => $reason, 'ContactInfo' => env("APP_URL") );
            $pdf->setSignature($certificate, $certificate, 'information', '', 1, $info, true);
        }
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $pdf->SetProtection($permission, $userpassword, $ownerpassword, 0, null);
        $pdf->numPages = $pdf->setSourceFile($inputPath);
        foreach(range(1, $pdf->numPages, 1) as $page) {
            try {
              $pdf->_tplIdx = $pdf->importPage($page);
            }
            catch(\Exception $e) {
              return false;
            }
            $size = $pdf->getTemplateSize($pdf->_tplIdx);
            $pdf->AddPage(self::orientation($size['w'], $size['h']), array($size['w'], $size['h']), true);
            $pdf->useTemplate($pdf->_tplIdx);
        }
        $pdf->Output($outputPath, 'F');
        $latestHash  = hash_file('sha512' , $outputPath);
        Database::table("files")->where("document_key", $document_key)->update(array("filename" => $outputName, "latest_hash" => $latestHash));

        $clientIP = self::get_client_ip();
        $activity = '<span class="text-primary">'.escape($user->fname.' '.$user->lname).'['.$clientIP.']</span> has activated document protection.'; 
        self::keephistory($document_key, $activity, "danger");
        self::deletefile($document->filename, true);
        self::keepcopy($outputName);
        return true;
    }
    
    /**
     * Sign & Edit document
     * 
     * @param   string $document_key
     * @return  array
     */
    public static function sign($document_key, $actions, $docWidth, $signing_key, $public = false, $is_initial = 0) {
        if (!empty($signing_key) && !$public) {
            $request = Database::table("requests")->where("signing_key", $signing_key)->first();
            $sender = Database::table("users")->where("id", $request->sender)->first();
            $userName = $request->email;
            $user = Auth::user();
            $userName = $user->fname.' '.$user->lname;
            $signature = config("app.storage")."signatures/".$user->signature;
        }else if ($public) {
            $userName = "Guest";
            $signature = null;
        }else{
            $user = Auth::user();
            $userName = $user->fname.' '.$user->lname;
            $signature = config("app.storage")."signatures/".$user->signature;
        }
        
        $document = Database::table("files")->where("document_key", $document_key)->first();
        $pdf = new PDF(null, 'px');
        $pdf->SetAutoPageBreak(FALSE, PDF_MARGIN_BOTTOM);
        $inputPath = config("app.storage")."files/".$document->filename;
        $outputName = Str::random(32).".pdf";
        $outputPath = config("app.storage")."/files/". $outputName;
        $pdf->numPages = $pdf->setSourceFile($inputPath);
        $actions = json_decode(base64_decode($actions), true);

        $templateFields = array($docWidth);        
        $signed = $updatedFields = $editted = false;
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        if (env("PKI_STATUS") == "Enabled") {
            $certificate = 'file://'.realpath(config("app.storage").'/credentials/tcpdf.crt');
            $reason = $document->sign_reason.' • Digital Signature | '.$userName.', '.self::ipaddress().','.date("F j, Y H:i");
            $info = array( 'Name' => $userName,  'Location' => env("APP_URL"), 'Reason' => $reason, 'ContactInfo' => env("APP_URL") );
            $pdf->setSignature($certificate, $certificate, 'information', '', 1, $info, true);
        }
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        foreach(range(1, $pdf->numPages, 1) as $page) {
            $rotate = false;
            $degree = 0;
            try {
              $pdf->_tplIdx = $pdf->importPage($page);
            }
            catch(\Exception $e) {
              return false;
            }
            foreach($actions as $action) {
                if(((int) $action['page']) === $page && $action['type'] == "rotate") {
                    $rotate = $editted = true;
                    $degree = $action['degree'];
                    break;
                }
            }
            $size = $pdf->getTemplateSize($pdf->_tplIdx);
            $scale = round($size['w'] / $docWidth, 3);

            $pdf->AddPage(self::orientation($size['w'], $size['h']), array($size['w'], $size['h'], 'Rotate'=>$degree), true);
            $pdf->useTemplate($pdf->_tplIdx);

            foreach($actions as $action) {
                if(((int) $action['page']) === $page) {
                    if ($action['group'] == "input") {
                        $updatedFields = true;
                        $templateFields[] = $action;
                        continue;
                    }elseif ($action['type'] == "image") {
                        $editted = true;
                        $imageArray = explode( ',', $action['image'] );
                        $imgdata = base64_decode($imageArray[1]);
                        $pdf->Image('@'.$imgdata, self::scale($action['xPos'], $scale), self::scale($action['yPos'], $scale), self::scale($action['width'], $scale), self::scale($action['height'], $scale), '', '', '', false);
                    }elseif ($action['type'] == "symbol" || $action['type'] == "shape" || $action['type'] == "stamp") {
                        $editted = true;
                        $svg = str_replace("%22", '"', $action['image']);
                        $pdf->ImageSVG('@'.$svg, self::scale($action['xPos'], $scale), self::scale($action['yPos'], $scale), self::scale($action['width'], $scale), self::scale($action['height'], $scale), '', '', '', 0, false);
                    }else if ($action['type'] == "drawing") {
                        $editted = true;
                        $imageArray = explode( ',', $action['drawing'] );
                        $imgdata = base64_decode($imageArray[1]);
                        $pdf->Image('@'.$imgdata, 0, 0, $size['w'], $size['h'], '', '', '', false);
                    }else if ($action['type'] == "qrcode") {
                        $qrcode = config("app.storage")."qrcode/$document_key.png";
                        $pdf->Image($qrcode, self::scale($action['xPos'], $scale), self::scale($action['yPos'], $scale), self::scale($action['width'], $scale), self::scale($action['height'], $scale), '', '', '', false);
                    }else if ($action['type'] == "signature") {
                        
                        $signed = true;
                        if (!$public) {
                            $pdf->Image($signatureNEW, self::scale($action['xPos'], $scale), self::scale($action['yPos'], $scale), self::scale($action['width'], $scale), self::scale($action['height'], $scale), '', '', '', false);
                        }else{
                            $imageArray = explode( ',', $action['image'] );
                            $imgdata = base64_decode($imageArray[1]);
                            $pdf->Image('@'.$imgdata, self::scale($action['xPos'], $scale), self::scale($action['yPos'], $scale), self::scale($action['width'], $scale), self::scale($action['height'], $scale), '', '', '', false);
                        }
                    }elseif ($action['type'] == "text") {
                        $editted = true;
                        // echo "<pre>"; print_r($action); die();
                        $pdf->SetFont($action['font'], $action['bold'].$action['italic'], $action['fontsize'] - 3);
                        // aealarabiya,aefurat,
                        // freeserif
                        /*$pdf->SetFont("aefurat", "", 18);*/
                        $pdf->writeHTMLCell( self::scale($action['width'] + 50, $scale), self::scale($action['height'], $scale), self::scale($action['xPos'], $scale) - 3, self::scale($action['yPos'], $scale), str_replace("%22", '"', $action['text']), 0, 0, false, true, '', true );
                    }
                }
            }
        }
        $pdf->Output($outputPath, 'F');
        if (count($templateFields) > 1) {
            Database::table("files")->where("document_key", $document_key)->update(array("filename" => $outputName, "editted" => "Yes", "template_fields" => json_encode($templateFields)));
        }else{
            Database::table("files")->where("document_key", $document_key)->update(array("filename" => $outputName, "editted" => "Yes"));
        }
        if (!empty($signing_key)) {
            $request = Database::table("requests")->where("signing_key", $signing_key)->first();
            $sender = Database::table("users")->where("id", $request->sender)->first();
            /*Database::table("requests")->where("signing_key", $signing_key)->update(array("status" => "Signed", "update_time" => date("Y-m-d H-i-s"))); */
            /* comment above line due to update_time not proper for all user */
            Database::table("requests")->where("signing_key", $signing_key)->update(array("status" => "Signed"));
            $notification = '<span class="text-primary">'.escape($userName).'</span> accepted a signing invitation of this <a href="'.url("Document@open").$request->document.'">document</a>.';
            Testclass::notification($sender->id, $notification, "accept", $request->document);
            $documentLink = env("APP_URL")."/document/".$request->document;
            $send = Mail::send(
                $sender->email, "Signing invitation accepted by ".$userName,
                array(
                    "title" => "Signing invitation accepted.",
                    "subtitle" => "Click the link below to view document.",
                    "buttonText" => "View Document",
                    "buttonLink" => $documentLink,
                    "message" => $userName." has accepted and signed the signing invitation you had sent. Click the link above to view the document.<br><br>Cheers!<br>".env("APP_NAME")." Team"
                ),
                "withbutton"
            );
            if(!empty($request->chain_emails)){
                self::nextRecipient($request->id);
            }
        }
        $clientIP = self::get_client_ip();
        if ($updatedFields) { 
            $activity = '<span class="text-primary">'.escape($userName).'['.$clientIP.']</span> updated template fields document.'; 
            self::keephistory($document_key, $activity, "default");
        }
        if ($editted) {
            $activity = '<span class="text-primary">'.escape($userName).'['.$clientIP.']</span> editted the document.'; 
            self::keephistory($document_key, $activity);
        }
        if ($signed) { 
            Database::table("files")->where("document_key", $document_key)->update(array("status" => "Signed"));
            $activity = '<span class="text-primary">'.escape($userName).'['.$clientIP.']</span> signed the document.'; 
            self::keephistory($document_key, $activity, "success");
        }
        self::deletefile($document->filename, "original");
        self::renamecopy($document->filename, $outputName);

        $fileSize = round(filesize($outputPath) / 1000);

        $latestHash  = hash_file('sha512' , $outputPath);
        $Query = (string)'UPDATE `files` SET `latest_hash`="'.$latestHash.'" WHERE `document_key`="'.$document_key.'"';
        $Query2 = (string)'UPDATE `files` SET `size`="'.$fileSize.'" WHERE `document_key`="'.$document_key.'"';
        Database::table('files')->queryFire($Query);
        Database::table('files')->queryFire($Query2);

        return true;
    }
    
    /**
     * Scale element dimension
     * 
     * @param   int $dimension
     * @return  int
     */
    public static function scale($dimension, $scale) {
        return round($dimension * $scale);
    }
    
    /**
     * Scale position on axis
     * 
     * @param   int $position
     * @return  int
     */
    public static function adjustPositions($position) {
        return round($position - 83);
    }
    
    /**
     * Get Ip Address
     * 
     * @param   int $position
     * @return  int
     */
    public static function ipaddress() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
     
        return $ipaddress;
    }
    
    /**
     * Send the next request on queue
     * 
     * @param   int $requestId
     * @return  true
     */
    public static function nextRecipient($requestId) {
        $request = Database::table("requests")->where("id", $requestId)->first();
        $sender = Database::table("users")->where("id", $request->sender)->first();
        $emails = json_decode($request->chain_emails, true);
        $chain_positions = str_replace( array('["[',']","[',']"]'), array('[[', '],[', ']]'), $request->chain_positions );
        $positions = json_decode($chain_positions, true);
        $activity = 'Signing request (Chained) sent to <span class="text-primary">'.escape($emails[0]).'</span> by <span class="text-primary">'.escape($sender->fname.' '.$sender->lname).'['.$clientIP.']</span>.';
        self::keephistory($request->document, $activity, "default");
        $chainEmails = $chainPositions = '';
        
        if(count($emails) > 1){
            $chainEmails = $emails;
            unset($chainEmails[0]);
            $chainEmails = array_values ( $chainEmails );
            $chainEmails = json_encode($chainEmails);
            $chainPositions = $positions;
            unset($chainPositions[0]);
            $chainPositions = array_values ( $chainPositions );
            $chainPositions = json_encode($chainPositions);
        }
        
        $signingKey = Str::random(32);
        if( env('GUEST_SIGNING') == "Enabled" AND env('FORCE_GUEST_SIGNING') == "Enabled"){
            $signingLink = env("APP_URL")."/view/".$request->document."?signingKey=".$signingKey;
        }else{
            $signingLink = env("APP_URL")."/document/".$request->document."?signingKey=".$signingKey;
        }
        $trackerLink = env("APP_URL")."/mailopen?signingKey=".$signingKey;
        $receiverData = Database::table("users")->where("email", $emails[0])->first();
        if (!empty($receiverData)) { $receiver = $receiverData->id; }else{ $receiver = 0; }
        $requestData = array( "sender_note" => escape($request->sender_note),  "chain_emails" => $chainEmails, "chain_positions" => $chainPositions, "company" => $sender->company, "document" => $request->document, "signing_key" => $signingKey, "positions" => json_encode($positions[0]), "email" => $emails[0], "sender" => $sender->id, "receiver" => $receiver );
        Database::table("requests")->insert($requestData);
        $send = Mail::send(
            $emails[0], $sender->fname." ".$sender->lname." has invited you to sign a document",
            array(
                "title" => "Document Signing invite",
                "subtitle" => "Click the link below to respond to the invite.",
                "buttonText" => "Sign Now",
                "buttonLink" => $signingLink,
                "message" => "You have been invited to sign a document by ".$sender->fname." ".$sender->lname.". Click the link above to respond to the invite.<br><strong>Message:</strong> ".$request->sender_note."<br><br>Cheers!<br>".env("APP_NAME")." Team
                <img src='".$trackerLink."' width='0' height='0'>"
            ),
            "withbutton"
        );
        return true;
    }


    /**
     * Create file
     * 
     * @param   int $data
     * @return  true
     */
    public static function create($createName = '', $folder = 1 ,$head = 0, $number = 1, $barcode = 0, $htitle = '', $hstring = '', $logoName= '') {

        $user = Auth::user();

        if ($user->role == "user") {
            $fileUsage = Database::table("files")->where("uploaded_by" , $user->id)->count("id", "files")[0]->files;
            $diskUsage = Database::table("files")->where("uploaded_by" , $user->id)->sum("id", "size")[0]->size / 1000;
            // check file usage limits
            if ($fileUsage > env("PERSONAL_FILE_LIMIT")) {
                exit(json_encode(responder("error", "Limit Exceeded!", "You have exceeded your limit of ".env("PERSONAL_FILE_LIMIT")." files.")));
            }
            // check disk usage limits
            if ($diskUsage > env("PERSONAL_DISK_LIMIT")) {
                exit(json_encode(responder("error", "Limit Exceeded!", "You have exceeded your limit of ".env("PERSONAL_DISK_LIMIT")." MBs.")));
            }
        }else{
            $fileUsage = Database::table("files")->where("company" , $user->company)->count("id", "files")[0]->files;
            $diskUsage = Database::table("files")->where("company" , $user->company)->sum("id", "size")[0]->size / 1000;
            // check file usage limits
            if ($fileUsage > env("BUSINESS_FILE_LIMIT")) {
                exit(json_encode(responder("error", "Limit Exceeded!", "You have exceeded your limit of ".env("BUSINESS_FILE_LIMIT")." files.")));
            }
            // check disk usage limits
            if ($diskUsage > env("BUSINESS_DISK_LIMIT")) {
                exit(json_encode(responder("error", "Limit Exceeded!", "You have exceeded your limit of ".env("BUSINESS_DISK_LIMIT")." MBs.")));
            }
        }
        if ($user->company == 0) {
            $files = Database::table("files")->where("name", $createName)->where("folder", $folder)->where("parent", $user->parent)->first();
        }else{
            $files = Database::table("files")->where("name", $createName)->where("folder", $folder)->where("company", $user->company)->first();
        }

        if (!empty($files)) {
            exit(json_encode(responder("error", "Already Exists!", "File name '".$createName."' already exists.")));
        }

        
        $fileName = Str::random(32).".pdf";
       // copy(config("app.storage")."app/create.pdf", config("app.storage")."files/".$fileName);
        //self::keepcopy($fileName);
        if(empty($createName)){ $createName = date("Ymdhis").".pdf"; }
        $clientIP = self::get_client_ip();
        $activity = 'File Create by <span class="text-primary">'.escape($user->fname.' '.$user->lname).'['.$clientIP.']</span>.';
       
                
            $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
             if(empty($user->author)){
               $creator = $user->fname.' '.$user->lname;  
             }else{
                 $creator = $user->author;
             }
               $title = $user->title;

            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetTitle($title);
            $pdf->SetAuthor($creator);
            $pdf->SetSubject($user->description);
           
            $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
            $pdf->setPrintHeader(true);
            $pdf->setPrintFooter(true);
                $meregqr = new MergeQR();
                $barcode = 1;
                $barcode = $meregqr->GetBarCode();
                // $QRpath = config("app.storage")."/qrcode/".$document_key.".png";
                // $string = env("APP_URL").url("track/$document_key");
                // $meregqr->generateQR($QRpath, $string, $barcode);
                $pdf->setBarcode($barcode);
            if ($head=='1') {
                //$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
                $pdf->SetHeaderData($logoName, PDF_HEADER_LOGO_WIDTH, $htitle, $hstring);
                $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
                $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
                $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
                $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
                // $pdf->setFooterData($barcode);
                // $pdf->write1DBarcode($barcode, 'EAN13', '5', '6', '60', 14, 0.4, $style, 'N');
 
            }
            
            for($i = 0; $i < $number; $i++){
                $pdf->AddPage();
                // draw jpeg image
                $pdf->Image(config("app.storage")."/app//watermark_grey.png", 20, 20, 1061, 1500, '', 'https://filesdna.com/', '', true, 72);

                $pdf->Image(config("app.storage").'signatures/'.$user->signature, 180, 257, 15, 15, 'PNG');
                //$pdf->Image($filePath, 180, 250, 15, 15, '');

                // restore full opacity
                $pdf->SetAlpha(1);
            }
                   
           // $pdf->Image(config("app.storage").'signatures/'.$user->signature, 20, 20, 1061, 1500, 'PNG');

             $filePath = config("app.storage").'signatures/'.$user->signature;
        //     $pdf->Image($filePath, 15, 15, 500, '', '', '', 'http://www.filesdna.com', false, 1000, '', false, false, 1, false, false, false);
            //$pdf->SetAlpha(1);
            // $pdf->setSignatureAppearance(15, 15, 500,1000);
            // $pdf->addEmptySignatureAppearance(15, 15, 500,1000);
           // $footer_image_file = './images/logo/footer.png';
          //  $footer_logo_html = '<div style="width:100px !important;height:100px; margin-right:200px;margin-top:1000px;"><img width:"1000px;" src="' . $filePath . '" /></div>';
          // $pdf->writeHTML($footer_logo_html, true, 0, true, 0);
          // $pdf->writeHTMLCell(21, '', 0, 29.7 - 4, $footer_logo_html, 0, 1, false, true, 'L', false);

        
            /*$pdf->SetXY(150, 200);
            $pdf->Image($filePath, '', '', 60, 60, '', '', 'T', false, 300, '', false, false, 1, false, false, false); */


            $outputPath = config("app.storage")."/files/". $fileName;
            $pdf->Output($outputPath, 'F');

        // $newPath = config("app.storage")."files/".$fileName;
        $hashValue = hash_file('sha512' , $outputPath);

        $data = array(
                        "company"       => $user->company,
                        "parent"        => $user->parent,
                        "uploaded_by"   => $user->id,
                        "name"          => $createName,
                        "folder"        => $folder,
                        "filename"      => $fileName,
                        "extension"     => 'pdf',
                        "size"          => round(filesize($outputPath) / 1000),
                        "status"        => 'Unsigned',
                        "is_template"   => 'No',
                        "document_key"  => Str::random(32),
                        "int_file_hash" => $hashValue,
                        "latest_hash"   => $hashValue,
                        "barcode"   => $barcode,
                    );
        Database::table("files")->insert($data);
        $documentId = Database::table("files")->insertId();
        $document = Database::table("files")->where("id", $documentId)->get("document_key");

        self::keephistory($document[0]->document_key, $activity);
        return $documentId;
    }

    public function imageConvert($filePath = "", $outputName = "") {

        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        /*// set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            require_once(dirname(__FILE__).'/lang/eng.php');
            $pdf->setLanguageArray($l);
        }*/

        // add a page
        $pdf->AddPage();

        // set JPEG quality
        $pdf->setJPEGQuality(75);

        $pdf->Image($filePath, 15, 15, 175, '', '', '', 'http://www.filesdna.com', false, 300, '', false, false, 1, false, false, false);
        
        // $pdf->Image($filePath, '15', '15', '175', '', 'jpg', '', '', true, 150, '', false, false, 0, false, false, false);
        
        $savePath = realpath(config("app.storage")."/files/").'/'.$outputName;

        $pdf->Output($savePath, 'F');
        
        return true;
    }

    public function textfileConvert($filePath = "", $outputName = "") {

        $pdf = new TCPDF();
        
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            require_once(dirname(__FILE__).'/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        $pdf->setFontSubsetting(true);

        $pdf->SetFont('freeserif', '', 14);

        $pdf->AddPage();

        $utf8text = file_get_contents($filePath, false);

        $pdf->Write(5, $utf8text, '', 0, '', false, 0, false, false, 0);

        $savePath = realpath(config("app.storage")."/files/").'/'.$outputName;
        $pdf->Output($savePath, 'F');
        
        return true;
    }

    public function imageConvertTest($filePath = "") {

        $filePath = "https://filesdna.com/uploads/files/33333.png";
        $filePath = "https://filesdna.com/uploads/files/xPWAvQA2zsiqILilt67Aw1JclaURYK6A.png";

        $pdf = new TCPDF();

        // -------------------------------------------------------------------

        // add a page
        $pdf->AddPage();

        // set JPEG quality
        $pdf->setJPEGQuality(75);

        $pdf->Image($filePath, 15, 15, 175, '', '', '', 'http://www.filesdna.com', false, 300, '', false, false, 1, false, false, false);

        // -------------------------------------------------------------------

        //Close and output PDF document
        $pdf->Output('example_009.pdf', 'I');
    }

}