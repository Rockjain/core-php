<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Create Digital signatures and Sign PDF documents online.">
    
    <link rel="icon" type="image/png" sizes="16x16" href="<?=url("");?>uploads/app/{{ env('APP_ICON'); }}">
    <title>Documents | Sign documents online</title>


    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.4.2/css/buttons.dataTables.min.css" />

    <!-- Ion icons -->
    <link href="<?=url("");?>assets/fonts/ionicons/css/ionicons.css" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link href="<?=url("");?>assets/libs/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="<?=url("");?>assets/libs/jquery-ui/jquery-ui.min.css" rel="stylesheet">
    <link href="<?=url("");?>assets/libs/select2/css/select2.min.css" rel="stylesheet">
    <link href="<?=url("");?>assets/css/simcify.min.css" rel="stylesheet">
    <!-- Signer CSS -->
    <link href="<?=url("");?>assets/css/style.css" rel="stylesheet">
    <script type="text/javascript">
          var developerKey = '<?php // echo  env("GOOGLE_API_KEY");?>';
          var clientId = '<?php // echo env("GOOGLE_CLIENT_ID");?>';
    </script>
    <!-- <script src="<?=url("");?>assets/js/googledriveimporter.js"></script> -->
    <script type="text/javascript" src="https://apis.google.com/js/api.js?onload=onApiLoad"></script>
</head>

<body>

    <!-- header start -->
    {{ view("includes/header", $data); }}

    <!-- sidebar -->
    {{ view("includes/sidebar", $data); }}

    <!-- <div class="content"> -->
    <div class="content">
        <div class="page-title documents-page" style="overflow:visible;">
            
            <div class="heading-title-btn-section">
			<h3>Documents</h3>
                <button class="btn btn-primary dropdown-toggle btn-responsive" data-toggle="modal" data-target="#uploadFile" data-backdrop="static" data-keyboard="false"> UPLOAD DOCs </button>
                <button class="btn btn-primary dropdown-toggle btn-responsive" data-toggle="modal" data-target="#CreateFile" data-backdrop="static" data-keyboard="false"> CREATE PDF </button>
                <button class="btn btn-primary dropdown-toggle btn-responsive" data-toggle="modal" data-target="#createFolder" data-backdrop="static" data-keyboard="false"> NEW FOLDER </button>
            </div>
            <p class="breadcrumbs text-muted"><span class="home-folder">Home Folder</span></p>
        </div>
        <div class="row">
            <div class="col-md-12 documents-group-holder">
                <div class="documents-filter light-card hidden-xs">
                    <div class="light-card-title">
                        <h4>Filter</h4>
                    </div>
                    <div class="documents-filter-form">
                        <form>
	                        <div class="col-md-4">
	                            <div class="form-group">
	                                <div class="row">
	                                    <div class="col-md-12">
	                                        <label class="radio"><input type="radio" name="status" value="" checked><span class="outer"><span class="inner"></span></span>All</label>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="form-group">
	                                <div class="row">
	                                    <div class="col-md-12">
	                                        <label class="radio"><input type="radio" name="status" value="Signed"><span class="outer"><span class="inner"></span></span>Signed</label>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="form-group">
	                                <div class="row">
	                                    <div class="col-md-12">
	                                        <label class="radio"><input type="radio" name="status" value="Unsigned"><span class="outer"><span class="inner"></span></span>Un-Signed</label>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
                            <div class="divider"></div>
                            <div class="col-md-4">
	                            <div class="form-group">
	                                <div class="row">
	                                    <div class="col-md-12">
	                                        <label class="radio"><input type="radio" name="type" value="" checked><span class="outer"><span class="inner"></span></span>All</label>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="form-group">
	                                <div class="row">
	                                    <div class="col-md-12">
	                                        <label class="radio"><input type="radio" name="type" value="files"><span class="outer"><span class="inner"></span></span>Files</label>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="form-group">
	                                <div class="row">
	                                    <div class="col-md-12">
	                                        <label class="radio"><input type="radio" name="type" value="folders"><span class="outer"><span class="inner"></span></span>Folders</label>
	                                    </div>
	                                </div>
	                            </div>
                            </div>
                            <div class="divider"></div>
	                        <div class="col-md-4">
	                            <div class="form-group">
	                                <div class="row">
	                                    <div class="col-md-12">
	                                        <label class="radio"><input type="radio" name="extension" value="" checked><span class="outer"><span class="inner"></span></span>All</label>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="form-group">
	                                <div class="row">
	                                    <div class="col-md-12">
	                                        <label class="radio"><input type="radio" name="extension" value="pdf"><span class="outer"><span class="inner"></span></span>PDF</label>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="form-group">
	                                <div class="row">
	                                    <div class="col-md-12">
	                                        <label class="radio"><input type="radio" name="extension" value="doc"><span class="outer"><span class="inner"></span></span>Word</label>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="form-group">
	                                <div class="row">
	                                    <div class="col-md-12">
	                                        <label class="radio"><input type="radio" name="extension" value="xls"><span class="outer"><span class="inner"></span></span>Excel</label>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="form-group">
	                                <div class="row">
	                                    <div class="col-md-12">
	                                        <label class="radio"><input type="radio" name="extension" value="ppt"><span class="outer"><span class="inner"></span></span>Power Point</label>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
                        </form>
                    </div>
                </div>

                <div class="row documents-grid">
                    <div class="dropdown">
                        

                        <button class="btn btn-primary dropdown-toggle btn-responsive"><a href="<?=url("Merge@get");?>"> MERGE PDFs </a></button>
                        <button class="btn btn-primary dropdown-toggle btn-responsive"><a href="<?=url("Settings@get");?>"> META DATAs </a></button>
                        <button class="btn btn-primary dropdown-toggle btn-responsive"><a  href="<?=url("Tag@get");?>"> TAGs </a></button>
                        <button class="btn btn-primary dropdown-toggle btn-responsive" id="filter-view"> FILTERS </button>
						 <button class="btn btn-primary dropdown-toggle btn-responsive" id="tag-search"> ADV. SEARCH </button>

                        <div class="pull-right">
						<button class="btn btn-primary dropdown-toggle btn-responsive" data-toggle="dropdown" aria-expanded="false" style="z-index: 160;"><i class="ion-arrow-down-b" ></i> <span id="item-count-view">{{ ($_COOKIE['item-count'])? $_COOKIE['item-count']:20 }}</span></button>
                        <ul class="dropdown-menu" style="min-width: 65px; z-index: 160;">
                            <li class="item-count" data-id="20">
                                <a href="" > <span>20</span></a>
                            </li>
                            <li class="item-count" data-id="35">
                                <a href="" > <span>35</span></a>
                            </li>
                            <li class="item-count" data-id="50">
                                <a href="" ><span> 50</span></a>
                            </li>
                            <li class="item-count" data-id="75">
                                <a href="" > <span>75</span></a>
                            </li>
                            <li class="item-count" data-id="100">
                                <a href="" > <span>100</span></a>
                            </li>
                        </ul>
                            @if($view_style == 0)
                                <img src="../assets/images/view_grid.png" width="25px" id="view_grid" class="view-change" style="display: none;" />
                                <img src="../assets/images/view_list.png" width="25px" id="view_list" class="view-change" />
                            @else
                                <img src="../assets/images/view_grid.png" width="25px" id="view_grid" class="view-change" />
                                <img src="../assets/images/view_list.png" width="25px" id="view_list" class="view-change" style="display: none;" />
                            @endif
                        </div>
                    </div>
                    <div class="col-md-12 content-list"><div class="loader-box"><div class="circle-loader"></div></div></div>
                    <div class="content-list-after"></div>
                </div>
            </div>
        </div>

    </div>

    <!-- footer -->
    {{ view("includes/footer"); }}

    <div class="select-option">
        <div class="btn-group btn-group-justified">
            <a href="" action="open" class="btn btn-primary" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="Open"><i class="ion-ios-eye"></i></a>
            @if ( in_array("delete",json_decode($user->permissions)) )
            <a href="" action="delete" class="btn btn-primary" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="Delete"><i class="ion-ios-trash"></i></a>
            @endif
            <a href="" action="rename" class="btn btn-primary" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="Rename"><i class="ion-edit"></i></a>
        </div>
    </div>


    <!-- Upload file Modal -->
    <div class="modal fade" id="uploadFile" role="dialog">
        <div class="close-modal" data-dismiss="modal">&times;</div>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Upload File</h4>
                </div>
                <form class="simcy-form" action="<?=url("Document@uploadfile");?>" data-parsley-validate=""  onsubmit="return ValidationEvent()" loader="true" method="POST">
                    <div class="modal-body">
                        <p>Only PDF<?php if(env("ALLOW_NON_PDF") == "Enabled"){ ?>, Word, Excel and Power Point, Txt, Jpg and Jpeg <?php } ?> allowed.</p>
                        <p>Please upload file with<?php if(env("FILE_UPLOAD_LIMIT") == "8"){ ?> max size 8 MB.  <?php } ?> </p>
                     
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>File name</label>
                                    <input type="text" class="form-control" name="name" placeholder="File name" data-parsley-required="true">
                                    <input type="hidden" name="folder" value="1">
                                    <input type="hidden" name="csrf-token" value="<?=csrf_token();?>" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                             <div class="row">
                                <div class="col-md-12">
                                    <label>Choose file</label>
                                    <input type="file" name="file" id="fUpload" class="dropify" data-parsley-required="true" data-allowed-file-extensions="pdf <?php if(env("ALLOW_NON_PDF") == "Enabled"){ ?>doc docx ppt pptx xls xlsx <?php } ?>">
                                    
                                </div>
                                <div id="fsize" style="text-align: center;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer update_sign_btn">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Upload file</button>
                    </div>
                </form>
            </div>

        </div>
    </div>

      
    <div class="modal fade" id="CreateFile" role="dialog">
        <div class="close-modal" data-dismiss="modal">&times;</div>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Create New Pdf File</h4>
                </div>
                <form class="simcy-form" action="<?=url("Document@createfile");?>" data-parsley-validate="" loader="true" method="POST" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>File name</label>
                                    <input type="text" class="form-control" name="fname" placeholder="File name" data-parsley-required="true">
                                    <input type="hidden" name="folder" value="1">
                                    <input type="hidden" name="csrf-token" value="<?=csrf_token();?>" />
                                </div>
                                <div class="col-md-12">
                                    <label>Include Header & Footer</label>
                                    <select class="form-control" name="head" id="headId">
                                      <option value="1">Yes</option>
                                      <option value="0">No</option>
                                    </select>
                                </div>
                                <div class="col-md-12">
                                    <label>Number Of Page</label>
                                    <input type="text" class="form-control" name="number" placeholder="Number of page" value="1" data-parsley-required="true">
                                </div>
                                
                            <div class="headerYes">
                                <div class="col-md-12">
                                    <label>Header Title</label>
                                    <input type="text" class="form-control" name="hTitle" placeholder="Header Title" data-parsley-required="true">
                                </div>
                                <div class="col-md-12">
                                    <label>Header String</label>
                                    <input type="text" class="form-control" name="hString" placeholder="Header String" data-parsley-required="true">
                                </div>
                                <div class="col-md-12">
                                    <label>Choose Header Logo</label>
                                    <input type="file" name="file" class="dropify logofile" data-parsley-required="true">   
                                </div>
                            </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer update_sign_btn">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Create file</button>
                    </div>
                </form>
            </div>

        </div>
    </div>


    <!-- Create folder Modal -->
    <div class="modal fade" id="createFolder" role="dialog">
        <div class="close-modal" data-dismiss="modal">&times;</div>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Create Folder</h4>
                </div>
                <form class="simcy-form" action="<?=url("Document@createfolder");?>" data-parsley-validate="" loader="true" method="POST">
                    <div class="modal-body">
                        <p class="text-muted">Use folders to organise your files.</p>
                        <div class="form-group">
                             <div class="row">
                                <div class="col-md-12">
                                    <label>Folder name</label>
                                    <input type="text" class="form-control" name="name" placeholder="Folder name" data-parsley-required="true">
                                    <input type="hidden" name="folder" value="1">
                                    <input type="hidden" name="csrf-token" value="<?=csrf_token();?>" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer update_sign_btn">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button class="btn btn-primary" type="submit">Create Folder</button>
                    </div>
                </form>
            </div>

        </div>
    </div>


    <!-- Rename folder Modal -->
    <div class="modal fade" id="renamefolder" role="dialog">
        <div class="close-modal" data-dismiss="modal">&times;</div>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Rename Folder</h4>
                </div>
                <form class="simcy-form" action="<?=url("Document@updatefolder");?>" data-parsley-validate="" loader="true" method="POST">
                    <div class="modal-body">
                        <p class="text-muted">Change the name of your folder.</p>
                        <div class="form-group">
                             <div class="row">
                                <div class="col-md-12">
                                    <label>Folder name</label>
                                    <input type="text" class="form-control" name="foldername" placeholder="Folder name" data-parsley-required="true">
                                    <input type="hidden" name="folder" value="1">
                                    <input type="hidden" name="folderid">
                                    <input type="hidden" name="csrf-token" value="<?=csrf_token();?>" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer update_sign_btn">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button class="btn btn-primary" type="submit">Rename Folder</button>
                    </div>
                </form>
            </div>

        </div>
    </div>


    <!-- Rename file Modal -->
    <div class="modal fade" id="renamefile" role="dialog">
        <div class="close-modal" data-dismiss="modal">&times;</div>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Rename File</h4>
                </div>
                <form class="simcy-form" action="<?=url("Document@updatefile");?>" data-parsley-validate="" loader="true" method="POST">
                    <div class="modal-body">
                        <p class="text-muted">Change the name of your file.</p>
                        <div class="form-group">
                             <div class="row">
                                <div class="col-md-12">
                                    <label>File name</label>
                                    <input type="text" class="form-control" name="filename" placeholder="File name" data-parsley-required="true">
                                    <input type="hidden" name="folder" value="1">
                                    <input type="hidden" name="fileid">
                                    <input type="hidden" name="csrf-token" value="<?=csrf_token();?>" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer update_sign_btn">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button class="btn btn-primary" type="submit">Rename File</button>
                    </div>
                </form>
            </div>

        </div>
    </div>


    <!-- Import from dropbox -->
    <div class="modal fade" id="dropbox" role="dialog">
        <div class="close-modal" data-dismiss="modal">&times;</div>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Import From Dropbox</h4>
                </div>
                <div class="modal-body">
                    <p class="text-muted">Simply import files from your Dropbox account. Only PDF<?php if(env("ALLOW_NON_PDF") == "Enabled"){ ?>, Word, Excel and Power Point <?php } ?> allowed.</p>
                    <div class="dropbox-button-holder">
                        <div id="dropbox-container"></div>
                    </div>
                </div>
                <div class="modal-footer update_sign_btn">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <!-- Google drive import-->
    <div class="modal fade" id="google-drive" role="dialog">
        <div class="close-modal" data-dismiss="modal">&times;</div>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Import From Google Drive</h4>
                </div>
                <div class="modal-body">
                    <p class="text-muted">Simply import files from your Google Drive account. Only PDF<?php if(env("ALLOW_NON_PDF") == "Enabled"){ ?>, Word, Excel and Power Point <?php } ?> allowed. Also make sure you have <a href="https://support.google.com/drive/answer/2494822" target="_blank">turned on sharing on your document</a></p>
                    <div class="dropbox-button-holder">
                        <button class="btn btn-primary" type="button" id="auth" disabled><i class="ion-social-google-outline"></i> Import Now</button>
                        <div id="result"></div>
                    </div>
                </div>
                <div class="modal-footer update_sign_btn">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>


    <!-- Shared Modal -->
    <div class="modal fade" id="shared" role="dialog">
        <div class="close-modal" data-dismiss="modal">&times;</div>
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Update Information </h4>
                </div>
                <form class="shared-holder simcy-form" action="" data-parsley-validate="" loader="true" method="POST" enctype="multipart/form-data">
                    <div class="loader-box"><div class="circle-loader"></div></div>
                </form>
            </div>

        </div>
    </div>



    <!-- Share Modal -->
    <div class="modal fade" id="sharefile" role="dialog">
        <div class="close-modal" data-dismiss="modal">&times;</div>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Document Sharing</h4>
                </div>
                <div class="modal-body">
                    <p>Anyone with the link below can view and edit this document.</p>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <label>Sharing link</label>
                                <input type="text" id="foo" class="form-control sharing-link" placeholder="Sharing link" readonly="readonly">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer update_sign_btn">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary copy-link" data-clipboard-action="copy" data-clipboard-target="#foo">Copy Link</button>
                </div>
            </div>

        </div>
    </div>


    <!-- folder right click -->
    <div id="folder-menu" class="dropdown clearfix folder-actions">
        <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu" style="display:block;position:static;margin-bottom:5px;">
            <li><a tabindex="-1" action="open" href="">Open</a>
            </li>
            <li><a tabindex="-1" action="rename" href="">Rename</a>
            </li>
            <li><a tabindex="-1" action="protect" href="">Protect</a>
            </li>
            <li><a tabindex="-1" action="move" href="">Move</a>
            </li>
            <li><a tabindex="-1" action="archive" href="">Archive</a>
            </li>
            @if ( $user->role != "user" )
            <li><a tabindex="-1" action="access" href="">Accessibility</a>
            </li>
            @endif
            @if ( in_array("delete",json_decode($user->permissions)) )
            <li class="divider"></li>
            <li><a tabindex="-1" action="delete" href="">Delete</a>
            </li>
            @endif
        </ul>
    </div>

    <!--  file right click -->
    <div id="file-menu" class="dropdown clearfix file-actions">
        <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu" id="filemenuUL" style="display:block;position:static;margin-bottom:5px;">
            <li><a action="open" href="">Open</a>
            </li>
            <li><a action="rename" href="">Rename</a>
            </li>
            <li><a action="duplicate" href="">Duplicate</a>
            </li>
            <li><a action="move" href="">Move</a>
            </li>
            <li><a action="download" href="">Download</a>
            </li>
            <li><a action="archive" href="">Archive</a>
            </li>
            @if ( $user->role != "user" )
            <li><a tabindex="-1" action="access" href="">Accessibility</a>
            </li>
            @endif
            @if ( in_array("delete",json_decode($user->permissions)) )
            <li class="divider"></li>
            <li><a action="delete" href="">Delete</a>
            </li>
            @endif
        </ul>
    </div>


    <!-- scripts -->
    <script type="text/javascript">
        var dropboxExtesions = ['.pdf'<?php if(env("ALLOW_NON_PDF") == "Enabled"){ ?>, '.doc', '.docx', '.ppt', '.pptx', '.xls', '.xlsx'<?php } ?>],
              docType = "documents",
              appUrl = "<?=env("APP_URL");?>",
              openFileUrl = "<?=url("Document@open");?>",
              documentsUrl = "<?=url("Document@fetch");?>",
              templatesUrl = "<?=url("Template@fetch");?>",
              relocateDocumentsUrl = "<?=url("Document@relocate");?>",
              duplicateFileUrl = "<?=url("Document@duplicate");?>",
              archiveFileUrl = "<?=url("Document@archive");?>",
              archiveFolderUrl = "<?=url("Document@archivefolder");?>",
              deleteUrl = "<?=url("Document@delete");?>",
              deleteFileUrl = "<?=url("Document@deletefile");?>",
              deleteFolderUrl = "<?=url("Document@deletefolder");?>",
              folderProtectViewUrl = "<?=url("Document@updatefolderprotectview");?>",
              folderProtectUrl = "<?=url("Document@updatefolderprotect");?>",
              folderAccessViewUrl = "<?=url("Document@updatefolderaccessview");?>",
              folderAccessUrl = "<?=url("Document@updatefolderaccess");?>",
              fileAccessViewUrl = "<?=url("Document@updatefileaccessview");?>",
              fileAccessUrl = "<?=url("Document@updatefileaccess");?>",
              dropboxUrl = "<?=url("Document@dropboximport");?>",
              googledriveimportUrl = "<?=url("Document@googledriveimport");?>",
              allowNonPDF = '<?=env("ALLOW_NON_PDF")?>';
              csrf = '<?=csrf_token();?>';
              MoveViewUrl = "<?=url("Document@movefilefolderview");?>";
              MoveUrl = "<?=url("Document@movefilefolder");?>";
              viewStyle = "{{ $view_style }}";
    </script>
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.flash.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.print.min.js"></script>

    <script src="<?=url("");?>assets/js/jquery-3.2.1.min.js"></script>
    <script src="<?=url("");?>assets/libs/jquery-ui/jquery-ui.min.js"></script>
    <script src="<?=url("");?>assets/libs/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=url("");?>assets/js//jquery.slimscroll.min.js"></script>
    <script src="<?=url("");?>assets/libs/select2/js/select2.min.js"></script>
    <script src="<?=url("");?>assets/js/simcify.min.js"></script>
    <script src="<?=url("");?>assets/libs/clipboard/clipboard.min.js"></script>
    <!-- <script type="text/javascript" src="https://www.dropbox.com/static/api/2/dropins.js" id="dropboxjs" data-app-key="<?=env("DROPBOX_APP_KEY");?>"></script> -->

    <!-- custom scripts -->
    <!-- <script src="<?=url("");?>assets/js/dropboximporter.js"></script> -->
    <script src="<?=url("");?>assets/js/app.js"></script>
    <script src="<?=url("");?>assets/js/files.js?t=<?= time()?>"></script>
    <script type="text/javascript">
        $(document).on('change', '.dropify', function(e){
            var uploadFileName = e. target. files[0]. name;
            $("input[name=name]").val(uploadFileName.split('.').shift());
        })
    </script>
                    <script type="text/javascript">
                        function pages(page, e, th, c, cp, sortorder = 'DESC', sortcolumn = 'name'){
                            e.preventDefault();
                            $(".content-list").html('<div class="loader-box"><div class="circle-loader"></div></div>');
                            if (docType === "documents") {
                                filter = {};
                                password = "";
                                var posting = $.post(documentsUrl, { "folder": $("input[name=folder]").val(),  "search": $("input[name=search]").val(),  "status": $("input[name=status]:checked").val(), "type": $("input[name=type]:checked").val(), "extension": $("input[name=extension]:checked").val(), "password": password, "csrf-token": Cookies.get("CSRF-TOKEN"), "page": page, "c":c, "cp":cp, "sort-order": sortorder, "sort-column":sortcolumn });
                            }else{
                                var posting = $.post(templatesUrl, { "csrf-token": Cookies.get("CSRF-TOKEN") });
                            }
                            posting.done(function (documents) {
                                var obj = JSON.parse(documents);
                                $(".content-list").html(obj.html);
                                $(".content-list-after").html(obj.pagination);
                                $('[data-toggle="tooltip"]').tooltip();
                                dragAction();
                                $(".select-option").removeClass("show");
                                ListGridOnLoad(obj.view_style);
                            });
                        }

                        $(document).on("click", ".item-count", function(e){
						    e.preventDefault();
						    var sess = $(this).attr('data-id');
						    document.cookie = "item-count="+sess;
						    $(".pageLI.active a").trigger("click");
						    $("#item-count-view").html(sess);
						});

                        $(document).on('change', '#headId', function(e){
                            var head = $(this).val();
                            if (head=='1') {
                                $('.headerYes').show();
                                $("input[name=hTitle]").attr("data-parsley-required","true");
                                $("input[name=hString]").attr("data-parsley-required","true");
                                $(".logofile").attr("data-parsley-required","true");
                                
                            }else if(head=='0'){
                                $('.headerYes').hide();
                                $("input[name=hTitle]").removeAttr( "data-parsley-required" );
                                $("input[name=hString]").removeAttr( "data-parsley-required" );
                                $(".logofile").removeAttr( "data-parsley-required" );
                            }
                            
                            //$("input[name=name]").val(uploadFileName.split('.').shift());
                        })
                    </script>
                     <script type="text/javascript">

    $(document).ready(function(){
        $('#fUpload').change(function(){
               var fp = $("#fUpload");
               var lg = fp[0].files.length; // get length
               var items = fp[0].files;
               var fileSize = 0;
           if (lg > 0) {
               for (var i = 0; i < lg; i++) {
                   fileSize = fileSize+items[i].size; // get file size
                   var sizeInMB = (fileSize / (1024*1024)).toFixed(2);
                   if(fileSize=0){
                       $('#fsize').html('');
                   }else{
                    $('#fsize').html(sizeInMB + 'MB Out Of 8 MB');
                }
               } 
           }
        });
    });

    function ValidationEvent() {
        var fp = $("#fUpload");
               var lg = fp[0].files.length; // get length
               var items = fp[0].files;
               var fileSize = 0;
           if (lg > 0) {
               for (var i = 0; i < lg; i++) {
                   fileSize = fileSize+items[i].size; // get file size
                   var sizeInMB = (fileSize / (1024*1024)).toFixed(2);
                   if(fileSize>8){
                    alert('upload max 8 MB');
                       return false;
                        returnToPreviousPage();
                   }else{
                    return true;
                }
               } 
           }
    }

    </script>
</body>

</html>
